check1(n, a[]) [
    {Positive(a) Λ 1 <= n}
    {Positive(a) Λ Once(a,0) Λ 0 <= 0 Λ 0 <= n-1}
    i := 0
    ;
    {Positive(a) Λ Once(a,i) Λ 0 <= i Λ i <= n-1}
    while i < n-1 do 
        {Positive(a) Λ Once(a,i) Λ 0 <= i Λ i <= n-1 Λ i < n-1}
        {Positive(a) Λ Once(a,i) Λ 0 <= i Λ i < n-1}
        {Positive(a) Λ Once(a,i) Λ a[i] > 0 Λ 0 <= i Λ i < n-1}
        {Positive(a) Λ Once(a,i) Λ a[i] > 0 Λ 0 <= i Λ i < n-1}
        {Positive(a) Λ Once(a,i) Λ a[i] > 0 Λ i < i+1 Λ 0 <= i Λ i < n-1 Λ 0 <= i+1}
        {Positive(a) Λ Once(a,i) Λ a[i] > 0 Λ i < i+1 Λ 0 <= i Λ i < n-1 Λ 0 <= i+1 Λ Noapp(a,i,i+1)}
        j := i+1
        ;
        {Positive(a) Λ Once(a,i) Λ a[i] > 0 Λ i < j Λ 0 <= i Λ i < n-1 Λ 0 <= j Λ Noapp(a,i,j) Λ j <= n}
        while j < n do
            {Positive(a) Λ Once(a,i) Λ a[i] > 0 Λ i < j Λ 0 <= i Λ i < n-1 Λ 0 <= j Λ Noapp(a,i,j) Λ j <= n Λ j < n}
            {Positive(a) Λ Once(a,i) Λ a[i] > 0 Λ i < j Λ 0 <= i Λ i < n-1 Λ 0 <= j Λ Noapp(a,i,j) Λ j < n}
            {Positive(a) Λ Once(a,i) Λ a[i] > 0 Λ i < j Λ 0 <= i Λ i < n-1 Λ 0 <= j Λ Noapp(a,i,j) Λ j <= n-1}
            if a[i] = a[j] then
                {Positive(a) Λ Once(a,i) Λ a[i] > 0 Λ i < j Λ 0 <= i Λ i < n-1 Λ 0 <= j Λ Noapp(a,i,j) Λ j <= n-1 Λ a[i] = a[j]}
                {a[i] > 0 Λ i <> j Λ 0 <= i Λ i < n-1 Λ 0 <= j Λ Noapp(a,i,j) Λ j <= n-1 Λ a[i] = a[j]}
                {a[i] > 0 Λ i <> j Λ 0 <= i Λ i <= n-1 Λ 0 <= j Λ Noapp(a,i,j) Λ j <= n-1 Λ a[i] = a[j]}
                return a[i] 
            else 
                {Positive(a) Λ Once(a,i) Λ a[i] > 0 Λ i < j Λ 0 <= i Λ i < n-1 Λ 0 <= j Λ Noapp(a,i,j) Λ j <= n-1 Λ a[i] <> a[j]}
                skip
            ;
            {Positive(a) Λ Once(a,i) Λ a[i] > 0 Λ i < j Λ 0 <= i Λ i < n-1 Λ 0 <= j Λ Noapp(a,i,j) Λ j <= n-1 Λ a[i] <> a[j]}
            {Positive(a) Λ Once(a,i) Λ i < j Λ 0 <= i Λ i < n-1 Λ 0 <= j Λ Noapp(a,i,j) Λ j <= n-1 Λ a[i] <> a[j]}
            {Positive(a) Λ Once(a,i) Λ i < j+1-1 Λ 0 <= i Λ i < n-1 Λ 0 <= j+1-1 Λ Noapp(a,i,j+1-1) Λ j+1-1 <= n-1 Λ a[i] <> a[j+1-1]}
            j := j+1
            {Positive(a) Λ Once(a,i) Λ i < j-1 Λ 0 <= i Λ i < n-1 Λ 0 <= j-1 Λ Noapp(a,i,j-1) Λ j-1 <= n-1 Λ a[i] <> a[j-1]}
            {Positive(a) Λ Once(a,i) Λ i < j-1 Λ j-1 < j Λ 0 <= i Λ i < n-1 Λ 0 <= j-1 Λ Noapp(a,i,j-1) Λ j-1 <= n-1 Λ a[i] <> a[j-1]}
            {Positive(a) Λ Once(a,i) Λ i < j Λ 0 <= i Λ i < n-1 Λ 0 <= j Λ Noapp(a,i,j-1) Λ j-1 <= n-1 Λ a[i] <> a[j-1]}
            {Positive(a) Λ Once(a,i) Λ i < j Λ 0 <= i Λ i < n-1 Λ 0 <= j Λ Noapp(a,i,j) Λ j-1 <= n-1}
            {Positive(a) Λ Once(a,i) Λ i < j Λ 0 <= i Λ i < n-1 Λ 0 <= j Λ Noapp(a,i,j) Λ j <= n}
        ;
        {Positive(a) Λ Once(a,i) Λ i < j Λ 0 <= i Λ i < n-1 Λ 0 <= j Λ Noapp(a,i,j) Λ j <= n Λ !(j < n)}
        {Positive(a) Λ Once(a,i) Λ i < j Λ 0 <= i Λ i < n-1 Λ 0 <= j Λ Noapp(a,i,j) Λ j <= n Λ j >= n}
        {Positive(a) Λ Once(a,i) Λ i < j Λ 0 <= i Λ i < n-1 Λ 0 <= j Λ Noapp(a,i,j) Λ j = n}
        {Positive(a) Λ Once(a,i) Λ 0 <= i Λ i < n-1 Λ Noapp(a,i,n)}
        {Positive(a) Λ Once(a,i+1-1) Λ 0 <= i+1-1 Λ i+1-1 < n-1 Λ Noapp(a,i+1-1,n)}
        i := i+1
        {Positive(a) Λ Once(a,i-1) Λ 0 <= i-1 Λ i-1 < n-1 Λ Noapp(a,i-1,n)}
        {Positive(a) Λ Once(a,i) Λ 0 <= i-1 Λ i-1 < n-1}
        {Positive(a) Λ Once(a,i) Λ 0 <= i-1 Λ i < n}
        {Positive(a) Λ Once(a,i) Λ 0 <= i-1 Λ i <= n-1}
        {Positive(a) Λ Once(a,i) Λ 0 <= i-1 Λ i-1 <= i Λ i <= n-1}
        {Positive(a) Λ Once(a,i) Λ 0 <= i Λ i <= n-1}
    ;
    {Positive(a) Λ Once(a,i) Λ 0 <= i Λ i <= n-1 Λ !(i < n-1)}
    {Once(a,i) Λ 0 <= i Λ i <= n-1 Λ i >= n-1}
    {Once(a,i) Λ 0 <= i Λ i = n-1}
    {Once(a,n-1)}
    {Once(a,n-1) Λ Noapp(a,n-1,n)}
    {Once(a,n)}
    return 0
]

Once(a,i) : Τα πρωτα i στοιχεια του a εμφανιζονται ακριβως μια φορα στον πινακα a. 
Noapp(a,i,j): To a[i] δεν εμφανιζεται στις θεσεις k του πινακα a για καθε k με i<k<j.

Todo: Noapp(a,i,i+1)
Todo: Once(a,i-1) Λ Noapp(a,i-1,n) συνεπαγεται Once(a,i)
