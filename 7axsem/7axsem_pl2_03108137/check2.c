#pragma JessieIntegerModel(math)

typedef enum { false=0, true=1 } bool;

#define MAXN 100000000
#define MAXV 1000001

/*@ requires n > 0;
  @ requires \valid_range(a, 0, n-1);
  @ requires \forall integer k; 0 <= k <= n-1 ==> 1 <= a[k] <= MAXV-1;
  @ ensures \result >= 0;
  @ ensures \result >  0 ==> \exists integer k1, k2; 0 <= k1 < k2 <= n-1 && a[k1] == a[k2];
  @ ensures \result == 0 ==> \forall integer i; 0 <= i < n ==> \exists integer j; 0 <= j <= i && a[i] == a[j] ==> j == i;
  @*/
int check2 (int n, int a[]) {
  bool found[MAXV];
  int i;
  /*@ loop invariant 0 <= i <= MAXV;
    @ loop invariant \forall integer k; 0 <= k < i ==> found[k] == false;
    @ loop variant MAXV-i;
    @*/
  for (i=0; i<MAXV; i++) found[i] = false;
  /*@ loop invariant 0 <= i <= n;
    @ loop invariant \forall integer k; 0 <= k < n && found[a[k]] ==> \exists integer j; 0 <= j < i && a[j] == a[k]; 
    @ loop variant n-i;
    @*/
  for (i=0; i<n; i++) if (found[a[i]]) return a[i];
                      else found[a[i]] = true;
  return 0;
}
