#pragma JessieIntegerModel(math)
/*@ requires n>=1;
  @ requires \valid_range(a, 0, n-1);
  @ requires
  @     \forall integer k; 0 <= k <= n-1 ==> 0 < a[k] <= 1000000;
  @ ensures 0 <= \result <= 1000000;
  @ behavior success:
  @   requires
  @     n >= 2;
  @   requires
  @     \exists integer k1, k2; 0 <= k1 < k2 <= n-1 ==> a[k1] == a[k2];
  @   ensures 0 < \result <= 1000000;
  @ behavior failure1:
  @   requires
  @     n == 1;
  @   ensures \result == 0;
  @ behavior failure2:
  @   requires
  @     \forall integer k1, k2; 0 <= k1 < k2 <= n-1 ==> a[k1] != a[k2];
  @   ensures \result == 0;
  @*/
int check1 (int n, int a[]) {
  int i, j;

  /*@ loop invariant 0 <= i <= n-1;
    @ loop variant n-i-1;
    @*/
  for (i=0; i<n-1; i++) {
    /*@ loop invariant i+1 <= j <= n;
      @ loop variant n-j;
      @*/
    for (j=i+1; j<n; j++) {
        if (a[i] == a[j]) return a[i];
      }
    }
  return 0;
}
