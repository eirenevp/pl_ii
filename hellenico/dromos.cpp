#include <fstream>
#include <ostream>
#include <iostream>
#include <istream>
#include <cstdlib>

using namespace std;


int main()
{
    ifstream inpfile("trlights.in");
    ofstream outfile("trlights.out");

    char c;
    int red=0,green=0;
    
    for (int i=0;i<10;i++){
         inpfile >> c;
         if (c == 'r') red++;
         else green++;
    }

    if (red >= 5) outfile <<  "*" << endl;
    else if (green == 10) outfile << "****" << endl;
         else if (red % 2 == 0) outfile << "***" << endl;
              else outfile << "**" << endl;

    return 0;
}
