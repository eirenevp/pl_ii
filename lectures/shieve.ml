datatype 'a stream_ = Cons of 'a * 'a stream
withtype 'a stream = 'a stream_ SMLofNJ.Susp.susp

fun seqFrom i = Cons (i, SMLofNJ.Susp.delay (fn() => seqFrom (i+1)))

fun take 0 _ = []
  | take n (Cons(x, xs)) = (x :: (take (n-1) (SMLofNJ.Susp.force xs)))

fun seqFilter modulo (Cons(x, xs)) =
  case (x mod modulo) of
       0 => seqFilter modulo (SMLofNJ.Susp.force xs)
     | _ => Cons(x, SMLofNJ.Susp.delay (fn () => seqFilter modulo (SMLofNJ.Susp.force xs)))

fun sieve( Cons(x, xs) ) = Cons(x, SMLofNJ.Susp.delay (fn() => sieve (seqFilter x (SMLofNJ.Susp.force xs))))

fun primes n =
  let
    val seq = seqFrom 2
  in
    take n (sieve seq)
end
