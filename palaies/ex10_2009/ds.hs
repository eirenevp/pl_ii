{-
 - WHILE interpreter
 -
 - Name:      Petros Pantavos
 - A.M.:      03106167
 - Date:      14/3/2010
 - Compiler:  ghc
 - Version:   The Glorious Glasgow Haskell Compilation System, version 6.8.2
 - Usage:     Source code should be given through standard input.
 -            Output will be writen to standard output.
 -            Example:
 -            ./ds < sourcefile
 - Notes:     - A warning message is writen to standard output for every unitialized variable.
 -              Uninitialized variables are initialized to 0.
 -            - The given parser was used.
 -}

module Main where
import Char
import Data.HashTable
import Data.Maybe

-- Syntax

type Var = String

data N  =  Nzero | Nsucc N | Nvar Var
data B  =  Btrue | Bnot B | Blt N N
data C  =  Cskip | Cseq C C | Cfor N C | Cif B C C | Cwhile B C | Cassign Var N

-- Pretty-printing

instance Show N where
  showsPrec p Nzero = ("0" ++)
  showsPrec p (Nsucc n) = ("succ " ++) . showsPrec p n
  showsPrec p (Nvar x) = (x ++)

instance Show B where
  showsPrec p Btrue = ("true" ++)
  showsPrec p (Bnot b) = ("not " ++) . showsPrec p b
  showsPrec p (Blt n1 n2) = showsPrec p n1 . (" < " ++) . showsPrec p n2

instance Show C where
  showsPrec p Cskip = ("skip" ++)
  showsPrec p (Cseq c1 c2) =
    showParen (p > 0) (showsPrec 1 c1 . ("; " ++) . showsPrec 0 c2)
  showsPrec p (Cfor n c) =
    ("for " ++) . showsPrec 0 n . (" do " ++) . showsPrec 1 c
  showsPrec p (Cif b c1 c2) =
    ("if " ++) . showsPrec 0 b . (" then " ++) . showsPrec 1 c1 .
                                 (" else " ++) . showsPrec 1 c2
  showsPrec p (Cwhile b c) =
    ("while " ++) . showsPrec p b . (" do " ++) . showsPrec 1 c
  showsPrec p (Cassign x n) = (x ++) . (" := " ++) . showsPrec p n

-- Parsing

isVar x = all isAlpha x && not (x `elem` keywords)
  where keywords = ["zero", "succ", "true", "not", "skip",
                    "for", "if", "then", "else", "while", "do"]

instance Read N where
  readsPrec p s =
    [(Nzero, r)    |  ("0", r) <- lex s ] ++
    [(Nsucc n, r)  |  ("succ", t) <- lex s, (n, r) <- readsPrec 0 t ] ++
    [(Nvar x, r)   |  (x, r) <- lex s, isVar x ]

instance Read B where
  readsPrec p s =
    [(Btrue, r)      |  ("true", r) <- lex s ] ++
    [(Bnot b, r)     |  ("not", t) <- lex s, (b, r) <- readsPrec 0 t ] ++
    [(Blt n1 n2, r)  |  (n1, t1) <- readsPrec 0 s,
                        ("<", t2) <- lex t1,
                        (n2, r) <- readsPrec 0 t2 ]

instance Read C where
  readsPrec p s =
    [(Cskip, r)          |  ("skip", r) <- lex s] ++
    [(Cassign x n, r)    |  (x, t1) <- lex s, isVar x,
                            (":=", t2) <- lex t1,
                            (n, r) <- readsPrec 1 t2 ] ++
    readParen (p > 0) (\s ->
      [(Cseq c1 c2, r)   |  (c1, t1) <- readsPrec 1 s,
                            (";", t2) <- lex t1,
                            (c2, r) <- readsPrec 0 t2 ]) s ++
    readParen (p > 1) (\s ->
      [(Cif b c1 c2, r)  |  ("if", t1) <- lex s,
                            (b, t2) <-readsPrec 0 t1,
                            ("then", t3) <- lex t2,
                            (c1, t4) <- readsPrec 1 t3,
                            ("else", t5) <- lex t4,
                            (c2, r) <- readsPrec 1 t5 ]) s ++
    readParen (p > 1) (\s ->
      [(Cfor n c, r)     |  ("for", t1) <- lex s,
                            (n, t2) <- readsPrec 0 t1,
                            ("do", t3) <- lex t2,
                            (c, r) <- readsPrec 1 t3 ]) s ++
    readParen (p > 1) (\s ->
      [(Cwhile b c, r)   |  ("while", t1) <- lex s,
                            (b, t2) <- readsPrec 0 t1,
                            ("do", t3) <- lex t2,
                            (c, r) <- readsPrec 1 t3 ]) s

evaln Nzero s = do return 0

evaln (Nsucc n) s = do  t <- evaln n s
                        return (t+1)

evaln (Nvar var) s = do n <- Data.HashTable.lookup s var
                        if (isJust n)
                            then  return (fromJust n)
                            else  do  putStr "Warning, uninitialized variable "
                                      putStr var
                                      putStrLn ". Set to 0."
                                      insert s var 0
                                      return 0

evalb Btrue s = do return True

evalb (Bnot b) s = do t <- evalb b s
                      return (not t)

evalb (Blt n1 n2) s = do  t1 <- evaln n1 s
                          t2 <- evaln n2 s
                          return (t1 < t2)

loop n c s = do if n > 0
                  then do eval c s
                          loop (n-1) c s
                  else return ()

eval Cskip s = do return ()

eval (Cseq c1 c2) s = do  eval c1 s
                          eval c2 s

eval (Cfor n c) s = do  t <- evaln n s
                        loop t c s

eval (Cif b c1 c2) s = do tb <- evalb b s
                          if tb
                              then eval c1 s
                              else eval c2 s

eval (Cwhile b c) s = do  tb <- evalb b s
                          if tb
                              then do eval c s
                                      eval (Cwhile b c) s
                              else return()

eval (Cassign var n) s = do tn <- evaln n s
                            update s var tn
                            return ()
                          
--    Data.HashTable.lookup mht "hello"

-- Main function: parsing a statement and pretty-printing

main = do input <- getContents
          s <- new (==) hashString :: IO (HashTable String Int)
          let c :: C
              c = read input
          eval c s
          res <- Data.HashTable.lookup s "result"
          if isJust res
              then putStrLn (show (fromJust res))
              else putStrLn "There was no assignment to variable 'result'"

