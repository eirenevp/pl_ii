i := 0;
j := succ succ succ succ succ succ 0;
four := succ succ succ succ 0;
for j do
(
	if i < four then
		i := succ i
	else
    (
		skip;
		skip;
		t := 0;
		skip;
		while t < four do
		(
			i := succ i;
			skip;
			t := succ t
		)
	)
);
result := i
