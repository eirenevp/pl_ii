import urllib
import urllib2
import cookielib
import hashlib

#Initializations

zero = 1
solution = ''

#genInst generates an OISC Instruction. row shows the line where the last instruction was, reg and inst are the numbers of the instruction

def genInst(row,reg,inst): 
	global solution
	solution += "%d %d%c" % (reg,inst,10)
	return row+1


#genAddBu generates OISC code that adds source to both dest and bu. source becomes zero


def genAddBu(row,dest,source,bu):
	firstRow = row+1
	nextRow = row+5
	row = genInst(row,source,nextRow)
	row = genInst(row,dest,0)
	row = genInst(row,bu,0)
	row = genInst(row,zero,firstRow)
	return row

#genDouble generates OISC code that adds the double of source to dest. source becomes zero

def genDouble(row,dest,source):
	firstRow = row+1
	nextRow = row+5
	row = genInst(row,source,nextRow)
	row = genInst(row,dest,0)
	row = genInst(row,dest,0)
	row = genInst(row,zero,firstRow)
	return row

#genCode_1 : acc 0 becomes 1 and then "iterative squaring" algorithm 

def genCode_1(a):
	row = 0;
	acc = 0;
	twos = 2;
	oldTwos = 3;
	row = genInst(row,0,3)
	row = genInst(row,zero,1)
	if a != 0 :
		row=genInst(row,twos,0)
	while a > 0:
		if (a % 2) == 1:
			row = genAddBu(row,acc,twos,oldTwos)
			temp = twos;
			twos = oldTwos;
			oldTwos = temp;
		a /= 2;
		if a == 0:
			break
		row = genDouble(row,oldTwos,twos);
		temp = twos;
		twos = oldTwos;
		oldTwos = temp;

#genCode_2 : acc zero becomes 0 and then "add 1" iteratively

def genCode_2(a) :
	row = 0;
	row = genInst(row,0,3)
	row = genInst(row,zero,1)
	while a > 0 :
		a -= 1;
		row = genInst(row,0,0);

#genCode_3 : acc starts from 42 and then "add 1" iteratively

def genCode_3(a) :
	row = 0;
	a = a-42;
	while a > 0 :
		a -= 1 ;
		row = genInst(row,0,0);

#genCode_4 : acc 0 starts from 42 and then "iterative squaring" algorithm as in genCode_1 for the num-42  

def genCode_4(a):
	row = 0;
	acc = 0;
	twos = 2;
	oldTwos = 3;
	a = a - 42;
	if a != 0 :
		row= genInst(row, twos,0);
	while a > 0:
		if (a % 2) == 1:
			row = genAddBu(row,acc,twos,oldTwos)
			temp = twos;
			twos = oldTwos;
			oldTwos = temp;
		a /= 2;
		if a == 0:
			break
		row = genDouble(row,oldTwos,twos);
		temp = twos;
		twos = oldTwos;
		oldTwos = temp;

#genCode_5 : acc 0 starts from 42 and then "reduce by 1" iteratively

def genCode_5(a) :
	row = 0;
	a = 42-a;
	b = a;
	while a > 0 :
		a = a - 1 ;
		row = genInst(row,0,b+1);

#genCode_6 : add 42 iteratively and then add 1 for the next (num mod 42) 

def genCode_6(a) :
	row = 0;
	b = a / 42;
	target = b+3;
	row = genInst(row,0,target);	
	while b > 0 :
		b -= 1;
		row = genInst(row,2,0);
	row = genInst(row,1,1);
	curr = row+1;

	row = genInst(row,2,curr+3);
	row = genInst(row,0,0);
	row = genInst(row,zero,curr);

	c = a % 42;
	while c > 0 :
		c -= 1;
		row = genInst(row,0,0);

#genCode_7 : as in genCode_6 but here we reduce by 1 for the (42-num) mod 42 numbers

def genCode_7(a) :
	row = 0;
	b = a / 42 + 1;
	if a % 42 == 0 :
		b -= 1;
	target = b+3;
	row = genInst(row,0,target);	
	while b > 0 :
		b -= 1;
		row = genInst(row,2,0);
	row = genInst(row,1,1);
	curr = row+1;

	row = genInst(row,2,curr+3);
	row = genInst(row,0,0);
	row = genInst(row,zero,curr);

	curr = row + 1;
	c = (42 - a) % 42;
	target = curr + c;
	while c > 0 :
		c -= 1;
		row = genInst(row,0,target);


#Calculate the lines of each method and choose the best

def genCode(a) :
	code = 0;
	num = a;
	rows = 2;
	while a > 0 :
		if (a % 2) == 1 :
			rows += 4;
		a /= 2;
		if a == 0 :
			break
		rows += 4;
	best_rows = rows; 
	code=1;						
	if num + 2 < best_rows :
		code = 2;
		best_rows=num + 2;
	if num >= 42 : 
		if num - 42 < best_rows :
			code = 3;
			best_rows = num-42 ;
		a = num - 42;
		rows = 0;
		while a > 0 :
			if (a % 2) == 1 :
				rows += 4;
			a /= 2;
			if a == 0 :
				break		
			rows += 4;
		if rows < best_rows:
			code = 4;
			best_rows = rows
	else :
		if 42 - num < best_rows :
			code = 5;
			best_rows = 42-num ;
	if num >= 42 :
		rows = 2 + (num / 42) +3 + (num % 42);
		if rows < best_rows :
			code = 6;
			best_rows = rows ;

		rows = 2 + (num / 42) + 3 + (42 - num) % 42;
		if rows < best_rows :
			code = 7;
			best_rows = rows ;

#Run the winner generator

	if code == 1 :
		genCode_1(num);
	if code == 2 :
		genCode_2(num);
	if code == 3 :
		genCode_3(num);
	if code == 4 :
		genCode_4(num);
	if code == 5 :
		genCode_5(num);
	if code == 6 :
		genCode_6(num);
	if code == 7 :
		genCode_7(num);
	
		




URL = 'http://courses.softlab.ntua.gr/pl2/exercises/vmgame.php'
URL = 'http://localhost/pl2/index.php'

#Initialize cookies
jar = cookielib.FileCookieJar("cookies")
opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(jar))

#Get the problem statement page
r = opener.open(URL)
source = r.read()
r.close()

#Save md5sum of server
md5Start = source.find('<span class="md5sum">')
md5Start += 21
md5End = source.find('</span>',md5Start)
md5Server = source[md5Start:md5End]

#Read the wanted number
numStart = source.find('<span class="number">')
numStart += 21
numEnd = source.find('</span>',numStart)
num = int(source[numStart:numEnd])

#Generate the solution
solution = ''
genCode(num)


#Post the client's solution to the server
postSol={}
postSol["solution"]=solution
postSol["submit"]="Submit!"
postData = urllib.urlencode(postSol)
r = opener.open(URL,postData)
source = r.read()
r.close()

#Get the solution of the server
ssolStart = source.find('<pre>')
ssolStart += 5
ssolEnd = source.find('</pre>',numStart)
while ssolStart < ssolEnd and not source[ssolStart].isdigit() :
	ssolStart += 1
while ssolStart < ssolEnd and not source[ssolEnd-1].isdigit() :
	ssolEnd -= 1
ssol = source[ssolStart:ssolEnd]
if ssolStart < ssolEnd-1 :
	ssol += ("%c" % (10))


#Print the requested number
print num 

#Print the client's solution 
print solution+"END"

#Print the server's solution 
print md5Server
realmd5Server = hashlib.md5(ssol).hexdigest()

#If server has given false md5sum, print a cheat message
if md5Server != realmd5Server :
	print "The server cheated. The actual md5sum of its solution is:"
	print realmd5Server

print ssol+"END"

