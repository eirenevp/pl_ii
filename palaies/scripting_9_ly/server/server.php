<!DOCTYPE html PUBLIC
          "-//W3C//DTD XHTML 1.0 Transitional//EN"
          "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>The OISC number generation game!</title>
<style type="text/css">
<!--
body,td,th {
        font-family: Verdana, Arial, Helvetica, sans-serif;
        font-size: x-large;
        color: #CCCCCC;
}

body {
        background-color: #333399;
}

.title {
        font-family: "Courier New", Courier, monospace;
        font-weight: bold;
        font-size: 48px;
        color: #00FF66;
}

.question {color: #FFCC33}
.number {color: #FFFF33}
.md5sum {color: #FFCCFF}
.emph {color: #99ee99}
.alert {color: #ee77aa}

.result {
        color: #FFCC33;
        font-weight: bold;
}
.score {
	color: #FFFF33;
	font-weight: bold;
}

a:link {
        color: #CCFFFF;
 
}

a:visited {
        color: #CCFFFF;
}

input {
        background-color: #eeee66;
        color: #333399;
}
-->
</style>
</head>
<body>


<h1>The OISC number generation game!</h1>

<p><span class="question">Give me the smallest possible OISC program
   that generates the given number.</span></p>
<p align="center">program size = number of instructions<p>
<p>Don't worry, finding the optimal solution is definitely intractable
   and most probably you won't be able to match my solution &mdash;
   at least not automatically.  You just need to achieve a good score.</p>
<p>Scores are calculated as follows.
   Suppose your solution is <span class="emph">Y</span> instructions long
   and my solution is <span class="emph">M</span> instructions long.
   Then:</p>
<p align="center">
   <span class="emph">your score</span> = min(50*(<span class="emph">M</span>+1)/(<span class="emph">Y</span>+1), 100)<br />
   <span class="emph">my score</span> = min(10*(<span class="emph">Y</span>+1)/(<span class="emph">M</span>+1), 100)</p>

<hr />

<?php
// Begin session
session_start();

// Solution generator
$MYSCRIPT = 'scrf.py';

if (empty($_POST["submit"]) || $_POST["submit"] != "Submit!"){
	//Random selection of number
	$randmax=min(0xFFFFFFFF,getrandmax());
	$num = rand(0,$randmax);

	$num = 42 * rand(0,2);

	// Save the number
	$_SESSION['num']=$num;
?>

<p>The number is:
<span class="number"><?php echo $num; ?></span>.</p>

<p>My solution has md5sum:
   <span class="md5sum"><?php system("python '$MYSCRIPT' $num | md5sum | cut -d \\  -f 1 | tr -d '\\n\\r' "); ?></span><br />
<span class="question">Submit yours...</span></p>

<?php
// Post form for client's solution

?>
<form action="/pl2/index.php" id="f" name="f" method="post">
  <textarea name="solution" id="solution" rows="10" cols="60"></textarea><br />
  <input type="submit" name="submit" id="submit" value="Submit!" />
  <input type="reset" value="Reset" />
</form>

<?php
}
else{
	$num=$_SESSION['num'];
?>

<p>The number is:
<span class="number"><?php echo $num; ?></span>.</p>

<form action="/pl2/index.php" id="r" name="r" method="post">
<input type="hidden" id="reset" name="reset" value="reset">
<input type="submit" name="again" id="again" value="Again!">
</form>
<hr>

My solution is:
<pre><?php system("python '$MYSCRIPT' $num"); ?></pre>

<?php
}
?>
</body>
</html>

