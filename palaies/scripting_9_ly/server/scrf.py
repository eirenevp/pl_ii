import sys

zero = 1

def genInst(row,reg,inst):
	print reg,inst
	return row+1

def genAddBu(row,dest,source,bu):
	firstRow = row+1
	nextRow = row+5
	row = genInst(row,source,nextRow)
	row = genInst(row,dest,0)
	row = genInst(row,bu,0)
	row = genInst(row,zero,firstRow)
	return row

def genDouble(row,dest,source):
	firstRow = row+1
	nextRow = row+5
	row = genInst(row,source,nextRow)
	row = genInst(row,dest,0)
	row = genInst(row,dest,0)
	row = genInst(row,zero,firstRow)
	return row

def genCode_1(a):
	row = 0;
	acc = 0;
	twos = 2;
	oldTwos = 3;
	row = genInst(row,0,3)
	row = genInst(row,zero,1)
	if a != 0 :
		row=genInst(row,twos,0)
	while a > 0:
		if (a % 2) == 1:
			row = genAddBu(row,acc,twos,oldTwos)
			temp = twos;
			twos = oldTwos;
			oldTwos = temp;
		a /= 2;
		if a == 0:
			break
		row = genDouble(row,oldTwos,twos);
		temp = twos;
		twos = oldTwos;
		oldTwos = temp;

def genCode_2(a) :
	row = 0;
	row = genInst(row,0,3)
	row = genInst(row,zero,1)
	while a > 0 :
		a -= 1;
		row = genInst(row,0,0);


def genCode_3(a) :
	row = 0;
	a = a-42;
	while a > 0 :
		a -= 1 ;
		row = genInst(row,0,0);


def genCode_4(a):
	row = 0;
	acc = 0;
	twos = 2;
	oldTwos = 3;
	a = a - 42;
	if a != 0 :
		row= genInst(row, twos,0);
	while a > 0:
		if (a % 2) == 1:
			row = genAddBu(row,acc,twos,oldTwos)
			temp = twos;
			twos = oldTwos;
			oldTwos = temp;
		a /= 2;
		if a == 0:
			break
		row = genDouble(row,oldTwos,twos);
		temp = twos;
		twos = oldTwos;
		oldTwos = temp;

def genCode_5(a) :
	row = 0;
	a = 42-a;
	b = a;
	while a > 0 :
		a = a - 1 ;
		row = genInst(row,0,b+1);

def genCode_6(a) :
	row = 0;
	b = a / 42;
	target = b+3;
	row = genInst(row,0,target);	
	while b > 0 :
		b -= 1;
		row = genInst(row,2,0);
	row = genInst(row,1,1);
	curr = row+1;

	row = genInst(row,2,curr+3);
	row = genInst(row,0,0);
	row = genInst(row,zero,curr);

	c = a % 42;
	while c > 0 :
		c -= 1;
		row = genInst(row,0,0);



def genCode_7(a) :
	row = 0;
	b = a / 42 + 1;
	if a % 42 == 0 :
		b -= 1;
	target = b+3;
	row = genInst(row,0,target);	
	while b > 0 :
		b -= 1;
		row = genInst(row,2,0);
	row = genInst(row,1,1);
	curr = row+1;

	row = genInst(row,2,curr+3);
	row = genInst(row,0,0);
	row = genInst(row,zero,curr);

	curr = row + 1;
	c = (42 - a) % 42;
	target = curr + c;
	while c > 0 :
		c -= 1;
		row = genInst(row,0,target);


def genCode(a) :
	code = 0;
	num = a;
	rows = 2;
	while a > 0 :
		if (a % 2) == 1 :
			rows += 4;
		a /= 2;
		if a == 0 :
			break
		rows += 4;
	best_rows = rows; 
	code=1;						
	if num + 2 < best_rows :
		code = 2;
		best_rows=num + 2;
	if num >= 42 : 
		if num - 42 < best_rows :
			code = 3;
			best_rows = num-42 ;
		a = num - 42;
		rows = 0;
		while a > 0 :
			if (a % 2) == 1 :
				rows += 4;
			a /= 2;
			if a == 0 :
				break		
			rows += 4;
		if rows < best_rows:
			code = 4;
			best_rows = rows
	else :
		if 42 - num < best_rows :
			code = 5;
			best_rows = 42-num ;
	if num >= 42 :
		rows = 2 + (num / 42) +3 + (num % 42);
		if rows < best_rows :
			code = 6;
			best_rows = rows ;

		rows = 2 + (num / 42) + 3 + (42 - num) % 42;
		if rows < best_rows :
			code = 7;
			best_rows = rows ;

	if code == 1 :
		genCode_1(num);
	if code == 2 :
		genCode_2(num);
	if code == 3 :
		genCode_3(num);
	if code == 4 :
		genCode_4(num);
	if code == 5 :
		genCode_5(num);
	if code == 6 :
		genCode_6(num);
	if code == 7 :
		genCode_7(num);
	

genCode(int(sys.argv[1]))

