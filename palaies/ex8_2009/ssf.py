#!/usr/bin/python
import urllib
import re
import sys

line=[0]
line*=81
col=[0]
col*=81
block=[0]
block*=81
o2bo=[k/3*9+k%3 for k in range(9)]
diax='-'*(9+8)
round=0
unPrizes=0
totPrizes=0
prizes={}
flag = len(sys.argv)<2

for i in range(9):
	for j in range(9):
		line[i*9+j] = i*9
		col[i*9+j] = j
		block[i*9+j] = i/3*3*9+j/3*3

def printSudoku(s):
	for i in range(9):
		for j in range(9):
			print s[i*9+j],
		print

def solve():
	global sud
	try:
		k = sud.index(0)
	except ValueError:
		return True
	for cand in range(1,10):
		for	offset in range(9):
			if sud[block[k]+o2bo[offset]]==cand or sud[line[k]+offset]==cand or sud[col[k]+offset*9]==cand:
				break
		else:
			sud[k] = cand
			if solve():
				return True
			sud[k] = 0
	return False

url = "http://www.gamesforthebrain.com/game/sudoku/"
bonusurl = "http://www.gamesforthebrain.com/bonus/"
response = urllib.urlopen(url)
htmlSource = response.read()
response.close()

while True:
	round+=1
	n=htmlSource.find('name="score"')
	n=htmlSource.find('value="',n)
	n+=7
	m=htmlSource.find('"',n)
	score=htmlSource[n:m].strip()
	n=htmlSource.find('name="map"')
	n=htmlSource.find('value="',n)
	n+=7

	print '\nRound %s | Score %s | Prizes %s | Unique prizes %s' % (round,score,totPrizes,unPrizes)
	print diax
	params={}
	sud=htmlSource[n:n+81]
	params["map"]=sud
	printSudoku(sud)
	print '\nSolved:'
	print diax
	sud=map(eval,list(sud.replace('.','0')))
	solve()
	printSudoku(sud)
	for i in range(81):
		params['f%s' % (i+1)] = str(sud[i])
	params["mode"]="didAnswer"
	params["score"]=score
	data = urllib.urlencode(params)
	response = urllib.urlopen(url,data)
	page = response.read()
	response.close()
	n=page.find('name="score"')
	n=page.find('value="',n)
	n+=7
	m=page.find('"',n)
	score=page[n:m].strip()
	#print '\nMy new score is', score
	n=page.find('You won a ticket!')
	if n >=0 :
		print '\nI won a ticket!'
		totPrizes+=1
		n=page.find('monospace')
		n=page.find('>',n)
		m=page.find('<',n)
		ticket = page[n+1:m].strip()
		print 'My ticket is', ticket
		params={}
		params["ticket"]=ticket
		data = urllib.urlencode(params)
		response = urllib.urlopen(bonusurl,data)
		htmlSource = response.read()
		response.close()
		n=htmlSource.find('class="bonus"')
		n=htmlSource.find('<img',n)
		n=htmlSource.find('src',n)
		n=htmlSource.find('"',n)
		n+=1
		m=htmlSource.find('"',n)
		imgpath=htmlSource[n:m]
		n=htmlSource.find('>',m)
		n+=1
		m=htmlSource.find('</div>',n)
		info=re.sub('<[^<>]+>','',htmlSource[n:m].replace('<p>','\n').replace('<br />','\n'))
		info=re.sub('^\n','',info)+'\n'
		print 'My prize is:'
		print info
		if prizes.has_key(imgpath):
			print 'I had that already!'
		else:
			unPrizes+=1
			prizes[imgpath]=info
			fname=imgpath.split('/')[-1]
			urllib.urlretrieve(bonusurl+imgpath,fname)
			localFile = open(fname + '.txt', 'w')
			localFile.write(info)
			localFile.close()
			print 'Image saved in', fname
			print 'Info saved in', fname + '.txt'
			if flag:
				break
	params={}
	params["mode"]=''
	params["score"]=score
	data = urllib.urlencode(params)
	response = urllib.urlopen(url,data)
	htmlSource = response.read()
	response.close()

