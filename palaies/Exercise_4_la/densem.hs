{-Theodoros Lykouris, 03106002 (The comments are written in English for practice)
The parser used is the one uploaded in last year's moodle. As a matter of fact, the programs can only have the commands defined there. As the exercise is about denotational semantics, changing the parser wasn't judged as necessary. 
The interpreter is written in haskell and can be easily compiled with no extra flags (ghc densem.hs -o densem).
-}

module Main where
import Char

{-As a Symbol table, it was prefered to use a HashTable with a String as the key (the name of the variable) and Int as the val (its value, as all the variables are integer). Thus, we import here Data.HashTable.
Data.Maybe is also essential because the "Data.HashTable.lookup" function returns (Maybe x) which is (Nothing) if the record doesn't exist and (Just x) if there is such a record. Thus, we import it as well.
-}
import Data.HashTable
import Data.Maybe

-- Syntax
type Var = String

data N  =  Nzero | Nsucc N | Nvar Var {-| Nadd N N-}
data B  =  Btrue | Bnot B | Blt N N
data C  =  Cskip | Cseq C C | Cfor N C | Cif B C C | Cwhile B C | Cassign Var N

-- Pretty-printing

instance Show N where
  showsPrec p Nzero = ("0" ++)
  showsPrec p (Nsucc n) = ("succ " ++) . showsPrec p n
  showsPrec p (Nvar x) = (x ++)

instance Show B where
  showsPrec p Btrue = ("true" ++)
  showsPrec p (Bnot b) = ("not " ++) . showsPrec p b
  showsPrec p (Blt n1 n2) = showsPrec p n1 . (" < " ++) . showsPrec p n2

instance Show C where
  showsPrec p Cskip = ("skip" ++)
  showsPrec p (Cseq c1 c2) =
    showParen (p > 0) (showsPrec 1 c1 . ("; " ++) . showsPrec 0 c2)
  showsPrec p (Cfor n c) =
    ("for " ++) . showsPrec 0 n . (" do " ++) . showsPrec 1 c
  showsPrec p (Cif b c1 c2) =
    ("if " ++) . showsPrec 0 b . (" then " ++) . showsPrec 1 c1 .
                                 (" else " ++) . showsPrec 1 c2
  showsPrec p (Cwhile b c) =
    ("while " ++) . showsPrec p b . (" do " ++) . showsPrec 1 c
  showsPrec p (Cassign x n) = (x ++) . (" := " ++) . showsPrec p n

-- Parsing
isVar x = all isAlpha x && not (x `elem` keywords)
  where keywords = ["zero", "succ", "true", "not", "skip",
                    "for", "if", "then", "else", "while", "do"]

instance Read N where
  readsPrec p s =
    [(Nzero, r)       |  ("0", r) <- lex s ] ++
    [(Nsucc n, r)     |  ("succ", t) <- lex s, (n, r) <- readsPrec 0 t ] ++
    [(Nvar x, r)      |  (x, r) <- lex s, isVar x ] 

instance Read B where
  readsPrec p s =
    [(Btrue, r)      |  ("true", r) <- lex s ] ++
    [(Bnot b, r)     |  ("not", t) <- lex s, (b, r) <- readsPrec 0 t ] ++
    [(Blt n1 n2, r)  |  (n1, t1) <- readsPrec 0 s,
                        ("<", t2) <- lex t1,
                        (n2, r) <- readsPrec 0 t2 ]

instance Read C where
  readsPrec p s =
    [(Cskip, r)          |  ("skip", r) <- lex s] ++
    [(Cassign x n, r)    |  (x, t1) <- lex s, isVar x,
                            (":=", t2) <- lex t1,
                            (n, r) <- readsPrec 1 t2 ] ++
    readParen (p > 0) (\s ->
      [(Cseq c1 c2, r)   |  (c1, t1) <- readsPrec 1 s,
                            (";", t2) <- lex t1,
                            (c2, r) <- readsPrec 0 t2 ]) s ++
    readParen (p > 1) (\s ->
      [(Cif b c1 c2, r)  |  ("if", t1) <- lex s,
                            (b, t2) <-readsPrec 0 t1,
                            ("then", t3) <- lex t2,
                            (c1, t4) <- readsPrec 1 t3,
                            ("else", t5) <- lex t4,
                            (c2, r) <- readsPrec 1 t5 ]) s ++
    readParen (p > 1) (\s ->
      [(Cfor n c, r)     |  ("for", t1) <- lex s,
                            (n, t2) <- readsPrec 0 t1,
                            ("do", t3) <- lex t2,
                            (c, r) <- readsPrec 1 t3 ]) s ++
    readParen (p > 1) (\s ->
      [(Cwhile b c, r)   |  ("while", t1) <- lex s,
                            (b, t2) <- readsPrec 0 t1,
                            ("do", t3) <- lex t2,
                            (c, r) <- readsPrec 1 t3 ]) s


{-"value" is a function that finds the value of a N element, either by evaluating it or by searching at the hash table if the N element is a variable. If the variable hasn't been initialised in advance, an error message is printed and the variable is set at 0 and is added to the hash table.
The function requires the N element examined and the hash table in order to lookup for the variables. The result is IO Int as the whole function is a Monad.-}
value:: N-> (HashTable String Int)->IO Int
value (Nzero) _ = return 0
value (Nsucc n) table = do z <- (value n table)
                           return (1 + z)
value (Nvar x) table = do n <-Data.HashTable.lookup table x 
                          finding n x
                              where finding:: (Maybe Int)->String->IO Int
                                    finding (Just a) _  = return a
                                    finding (Nothing) x = do putStrLn ("Not initialised variable " ++ x ++ ". To set at 0.")
                                                             insert table x 0
                                                             return 0

{-"semantics" is the function that interprets the commands of the program. Its purpose is to refresh the symbol table as the command is interpreted. It requires the C element we want to interpret and the hash table mentioned above. The result is IO () as the function is a Monad and nothing needs to be returned.-}
semantics:: C->(HashTable String Int)->IO ()  

semantics (Cskip) table = return ()

{-"update" function is prefered to "insert" function so that we can avoid multiple entries.-}
semantics (Cassign x n) table = do z<- (value n table)
                                   update table x z 
                                   return ()

semantics (Cseq c1 c2) table = do semantics c1 table
                                  semantics c2 table       

semantics (Cif (Btrue) c1 c2) table  = semantics c1 table                                      
semantics (Cif (Bnot b) c1 c2) table = semantics (Cif b c2 c1) table
semantics (Cif (Blt n1 n2) c1 c2) table = do z1 <- (value n1 table)
                                             z2 <- (value n2 table)
                                             semantics (if (z1 < z2) then c1 else c2) table

semantics (Cfor n c) table = do   repeats <- (value n table)
                                  for_loop repeats c table
                                          where for_loop :: Int -> C -> (HashTable String Int)->IO ()
                                                for_loop 0 c table = return ()
                                                for_loop r c table = do semantics c table
                                                                        for_loop (r-1) c table
 
{- This implementation is described as false in the lecture, however, in fact, it seems to have the same results as the other
semantics (Cwhile b c) table = do bool_check <- check b table
                                  next_while b c table bool_check
                                          where check:: B -> (HashTable String Int)-> IO Bool
                                                check (Btrue) _ = return True
                                                check (Bnot b) table = do a <- check b table
                                                                          return (not a)
                                                check (Blt n1 n2) table = do z1 <- (value n1 table)
                                                                             z2 <- (value n2 table)
                                                                             return (z1 <z2)      

                                                next_while :: B -> C -> (HashTable String Int) -> Bool -> IO ()
                                                next_while b c table (True)  = do  semantics c table
                                                                                   semantics (Cwhile b c) table
                                                next_while b c table (False) = do return () 

-}

{-Semantics of while using fix-}
semantics (Cwhile b c) table = do fix (f_capital b c) table
                                  return ()
                                      
check:: B -> (HashTable String Int)-> IO Bool
check (Btrue) _ = return True
check (Bnot b) table = do a <- check b table
                          return (not a)
check (Blt n1 n2) table = do z1 <- (value n1 table)
                             z2 <- (value n2 table)
                             return (z1 <z2)      


fix f = f (fix f)

f_capital b c f table = do  bool_check <- check b table
                            if (bool_check) then do semantics c table
                                                    f table
                                                    return ()                                                 
                             else return ()




{-The main function interprets the whole program, uses "lookup" afterwards in order to take the value of "result" and prints it.-}
main = do  input <- getContents
           let c :: C
               c = read input
           table <- new (==) hashString :: IO (HashTable String Int)
           semantics c table
           res <- Data.HashTable.lookup table "result"
           finding res
                 where finding:: (Maybe Int)->IO ()
                       finding (Just a)  = print a
                       finding (Nothing) = do putStrLn ("No result found.")
                                              
