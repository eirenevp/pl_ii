{- (Comments are in Greek, iso8859-7)
 -
 - ÃñÜøôå ìßá "óõíÜñôçóç" ðïõ íá ìåôñÜåé ðüóåò öïñÝò Ý÷åé êëçèåß
 - êáé íá åðéóôñÝöåé ôçí ôéìÞ ôïõ ìåôñçôÞ.
 -}


-- Ï ôýðïò ôçò êáôÜóôáóçò (ìüíï ï ìåôñçôÞò)

type State = Int


-- Õëïðïßçóç ôïõ state monad

newtype StM a = MkM (State -> (a, State))

instance Monad StM where
  return x = MkM (\s -> (x, s))
  MkM m >>= f = MkM (\s -> let (a, s') = m s 
                               MkM m' = f a
                           in  m' s')

run :: State -> StM a -> a
run s0 (MkM m) = fst (m s0)


-- Ç æçôïýìåíç "óõíÜñôçóç"

nextCounter :: StM Int
nextCounter = MkM (\s -> (s, s+1))


-- Ôï êýñéï ðñüãñáììá ðïõ ôõðþíåé (0, 1, 2)

main =
  let stm = do n <- nextCounter
               m <- nextCounter
               k <- nextCounter
               return (n, m, k)
  in  print (run 0 stm)
