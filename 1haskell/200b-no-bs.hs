{-# OPTIONS_GHC -O2 -optc-O2 #-}

-- solution for: http://codeforces.com/problemset/problem/200/B

solve :: Int -> [Int] -> Float
solve n x = sum (map fromIntegral x) / fromIntegral n

main = 
  do n <- readLn
     str <- getLine
     let x = map read (words str)
     print (solve n x)
