{-# OPTIONS_GHC -O2 -optc-O2 #-}

-- solution for: http://codeforces.com/problemset/problem/200/B

import Data.Char (isSpace)
import qualified Data.ByteString as BS
import qualified Data.ByteString.Char8 as BSC

solve :: Int -> [Int] -> Float
solve n x = sum (map fromIntegral x) / fromIntegral n

main = 
  do all <- BS.getContents
     let Just (n, r1) = readInt all
     let (x, _)  = readMany readInt r1
     print (solve n x)
  where readInt s = BSC.readInt (BSC.dropWhile isSpace s)
        readInteger s = BSC.readInteger (BSC.dropWhile isSpace s)
        readMany readf s = case readf s of
          Just (x, r) -> let (xs, t) = readMany readf r
                         in  (x : xs, t)
          Nothing     -> ([], s)
