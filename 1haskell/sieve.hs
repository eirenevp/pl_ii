{- (Comments are in Greek, iso8859-7)
 -
 - ������ ��� ��������� ��� �� �������� ��� �� ������������ ���� ������ N
 - ��� �� ��������� ���� N ����������� ������� ��������.
 -}

primes = sieve [2..]
  where sieve (x:xs) = x : sieve [ y | y <- xs, y `mod` x /= 0 ]

main = do n <- readLn
          print (take n primes)
