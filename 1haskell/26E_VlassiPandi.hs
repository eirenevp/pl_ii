{-        Irene Vlassi-Pandi (03108137)       -}
{-              ex1-plII-2012                 -}
{- http://codeforces.com/contest/26/problem/E -}

{- algorithm by:                              -}
{- http://codeforces.com/blog/entry/610       -}

import Data.List
import Data.Char (isSpace)
import qualified Data.ByteString as BS
import qualified Data.ByteString.Char8 as BSC

multiH :: [Int] -> [Int] -> [[Int]] -> [Int]
multiH  [ ]     _     acc = reverse (concat acc)
multiH   _     [ ]    acc = reverse (concat acc)
multiH (m:ms) (n:ns)  acc = multiH ms ns (replicate (2*m) (n+1):acc)

multi :: Int -> Int -> [Int] -> [Int]
multi n w ls  
    | 1 > w || w > sum ls = []

multi 1 w [l] 
    | l == w = 
        replicate (2*w) 1
    | otherwise = []

multi n 1 ls
    | n >= 2 =   
        let
            js   = findIndices (==1)    ls 
            inds = findIndices (/=(-1)) ls
        in 
           if (js /= [])
           then
               let 
                   j = head js
               in
                   (j+1):multiH (delete 1 ls) (delete j inds) []++[j+1]
           else []

multi n w (l1:l2:ls) = 
    let
       multi2 n2 w2 (nl1:nl2:nls) inds 
        | w2 == 2 = 
            1:multiH [nl2] [1] []++multiH nls inds []++[1]++[2]++multiH [nl1] [0] []++[2] 
        | w2 >  2 =
            let
                findli _ n3 2   []        z1 z2  _     acc = reverse acc++multi2 n3 2 [z1,z2] [] 
                findli _ n3 w3  []        z1 z2  _     acc 
                    | z1  >  0 = findli  0    n3 (w3-1)   []          (z1-1) z2    [] (1:1:acc)
                    | z1  == 0 = findli  1    n3  w3      []           0     z2    []  acc 
                    | z2  >  0 = findli  1    n3 (w3-1)   []           z1   (z2-1) [] (2:2:acc)
                    | z2  == 0 = findli  2    n3  w3      []           z1    0     []  acc 
                findli _ n3 2   nnls      z1 z2 (i:is) acc = reverse acc++multi2 n3 2 (z1:z2:nnls) (i:is)   
                findli 0 n3 w3  nnls      z1 z2 (i:is) acc 
                    | z1  >  0 = findli  0    n3 (w3-1)   nnls         (z1-1) z2    (i:is)  (1:1:acc)
                    | z1  == 0 = findli  1    n3  w3      nnls          0     z2    (i:is)   acc 
                findli 1 n3 w3  nnls      z1 z2 (i:is) acc 
                    | z2  >  0 = findli  1    n3 (w3-1)   nnls          z1   (z2-1) (i:is)  (2:2:acc)
                    | z2  == 0 = findli  2    n3  w3      nnls          z1    0     (i:is)   acc 
                findli j n3 w3 (nnl:nnls) z1 z2 (i:is) acc      
                    | j   == n3 = reverse acc++multi2 n3 2 (z1:z2:nnl:nnls) (i:is) 
                    | nnl >  0 = findli  j    n3 (w3-1) ((nnl-1):nnls)  z1    z2    (i:is) ((i+1):(i+1):acc)
                    | nnl == 0 = findli (j+1) n3  w3      nnls          z1    z2     is      acc

            in
                findli 0 n2 w2 nls nl1 nl2 [2..(n2-1)] [] 
    in
       multi2 n w ((l1-1):(l2-1):ls) [2..(n-1)]


main = 
  do all <- BS.getContents
     let Just (n, r1) = readInt all
     let Just (w, r2) = readInt r1
     let (x, _)  = readMany readInt r2
     let answer = (multi n w x)
     print_yesno answer
     printList answer
  where readInt s = BSC.readInt (BSC.dropWhile isSpace s)
        readMany readf s = case readf s of
          Just (x, r) -> let (xs, t) = readMany readf r
                         in  (x : xs, t)
          Nothing     -> ([], s)
        print_yesno [] = putStrLn "No"
        print_yesno _  = putStrLn "Yes"
        printList []     = putStrLn""
        printList (l:[]) = 
          do
              putStrLn (show l)
        printList (l:ls) = 
          do
              putStr (show l++ " ")
              printList ls

