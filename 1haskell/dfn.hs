{- (Comments are in Greek, iso8859-7)
 -
 - ������� ��� ������.  �������� �� ������������� ��� ������ ��� �����
 - �� ���� ��������� �� ���� ����� ���� ������� �������: � ����� �� ���
 - ����� ��� �������� ���� ����� (depth-first) ������������ ��� �����.
 -}

data Tree a = T a [Tree a]
  deriving Show

dfn :: Tree a -> Tree (a, Int)
dfn t = fst (one (t, 1))
  where one (T x ts, n) = (T (x, n) ts', n')
          where (ts', n') = many (ts, n+1)
        many ([], n) = ([], n)
        many (t:ts, n) = (t':ts', n')
          where (t', m) = one (t, n)
                (ts', n') = many (ts, m)

t = T 'a' [T 'b' [T 'e' [], T 'f' [T 'h' []]],
           T 'c' [],
           T 'd' [T 'g' [T 'i' []]]]

main = print (dfn t)
