import Data.List


--myList = 1 : concatMap (\n -> 0 : []) myList

{-inloop (-1) b t best   d     = best
inloop   j  b t best  (d1:d2:ds) = 
    inloop (j-1) b t (max best (b-j))
-}
--dp (d:ds) = min 
makeList n = 
    let makeListH 1 acc = (1:acc)
        makeListH n acc = makeListH (n-1) (0:acc)
    in
        makeListH n []


updateH _ _ ans []   (reverse acc) = ans
updateH t d ans [l1] acc =
    let
        st = max (l+1) t
    in
        let 
            ans = max ans (st-l-1)
            l = st+d-1
        in
            updateH t d ans [] (l:acc)
updateH t d ans (l1:l2:ls)  acc =
    let
        st = max (l+1) t
    in
        let 
            ans = max ans (st-l-1)
            l = min (st+d-1) l2
        in
           updateH t d ans (l2:ls) (l:acc)

main n k t d = 
    let

updateH t d (-1) (makeList k) []
main n k t d = 
