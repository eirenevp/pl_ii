{- (Comments are in Greek, iso8859-7)
 -
 - ������� ��� ������.  �������� �� ������������� ��� ������ ��� �����
 - �� ���� ��������� �� ���� ����� ���� ������� �������: � ����� �� ���
 - ����� ��� �������� ���� ������ ((breadth-first) ������������ ��� �����.
 -}

data Tree a = T a [Tree a]
  deriving Show

bfn t = t'
  where (t', ns) = one (t, 1:ns) -- the magic knott!
        one (T x ts, n:ns) = (T (x, n) ts', (n+1):ns')
          where (ts', ns') = many (ts, ns)
        many ([], ns) = ([], ns)
        many (t:ts, ns) = (t':ts', ns')
          where (t', ms) = one (t, ns)
                (ts', ns') = many (ts, ms)

t = T 'a' [T 'b' [T 'e' [], T 'f' [T 'h' []]],
           T 'c' [],
           T 'd' [T 'g' [T 'i' []]]]

main = print (bfn t)
