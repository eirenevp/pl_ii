import Debug.Trace
import Data.List
import Data.Char
import System.IO

data Expr  = Econst Char
           | Eout Expr Expr
           | Ein Expr
           | Evar Char
           | Eabs Char Expr
           | Eapp Expr Expr
           deriving Eq

-- Parsing of expressions

next (h : t) = [(h, t)]
next s = []

instance Read Expr where
  readsPrec p s =
      [(Eapp e1 e2, r)  |  ('@', r1) <- next s,
                           (e1, r2) <- readsPrec 0 r1,
                           (e2, r) <- readsPrec 0 r2] ++
      [(Econst x, r)    |  ('#', r1) <- next s,
                           (x, r) <- next r1] ++
      [(Eabs x e, r)    |  ('/', r1) <- next s,
                           (x, r2) <- next r1,
                           (e, r) <- readsPrec 0 r2] ++
      [(Eout e1 e2, r)  |  ('+', r1) <- next s,
                           (e1, r2) <- readsPrec 0 r1,
                           (e2, r) <- readsPrec 0 r2] ++
      [(Ein e, r)       |  ('-', r1) <- next s,
                           (e, r) <- readsPrec 0 r1] ++
      [(Evar x, r)      |  (x, r) <- next s]

type S = ([Char], [Char])
type D = S -> (V, S)
data V = Vc { c :: Char } | Vd { d :: (V -> D) } | Ve { e :: D }
type Env = Char -> V

updateEnv env x y z = if z == x then y
                                else env z

run :: Expr -> Env -> D
run (Eout e1 e2) env state      = let (ret, (inp', outp')) = run e1 env state
                                  in run e2 env (inp', (c ret) : outp')
run (Ein e) env (h : inp, outp) = run (Eapp e (Econst h)) env (inp, outp)
run (Econst x) env state        = (Vc x, state)
run (Evar x) env state          = e (env x) state
run (Eabs x e) env state        = let abs = ( \ value -> run e (updateEnv env x value) )
                                  in (Vd abs, state)
run (Eapp e1 e2) env state      = let (ret, state') = run e1 env state
                                      denotated = ( \ stateparam -> run e2 env stateparam )
                                  in d ret (Ve denotated) state'

interpret d input = do  
    let (value, (_, output)) = d (input, "")  
    putStrLn $ reverse output  
    return value  
empty x = error ("uninitialized variable " ++ show x) 
main = do  
        input <- getContents  
        let (program : phrases) = lines input  
        mapM_ (interpret $ run (read program) empty) phrases  
