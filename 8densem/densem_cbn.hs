import Data.List
import Data.Char
import System.IO

data Expr  = Eabs Char Expr | Eapp Expr Expr | Evar Char 
           | Econst Char | Ein Expr | Eout Expr Expr 
           deriving Eq

next (x : y) = [(x, y)]
next x = []

instance Read Expr where
  readsPrec _ s =
      [(Eabs x e, r)    |  ('/', r1) <- next s,
                           (x, r2) <- next r1,
                           (e, r) <- readsPrec 0 r2] ++
      [(Eapp e1 e2, r)  |  ('@', r1) <- next s,
                           (e1, r2) <- readsPrec 0 r1,
                           (e2, r) <- readsPrec 0 r2] ++
      [(Evar x, r)      |  (x, r) <- next s] ++
      [(Econst x, r)    |  ('#', r1) <- next s,
                           (x, r) <- next r1] ++
      [(Ein e, r)       |  ('-', r1) <- next s,
                           (e, r) <- readsPrec 0 r1] ++
      [(Eout e1 e2, r)  |  ('+', r1) <- next s,
                           (e1, r2) <- readsPrec 0 r1,  
                           (e2, r) <- readsPrec 0 r2] 

data V = V1 { c :: Char } | V2 { d :: (V -> D) } | V3 { e :: D }
type D = S -> (V, S)
type S = ([Char], [Char])
type Env = Char -> V

update env x y z = if z == x then y else env z

walk :: Expr -> Env -> D
walk (Eabs x e) env st           = let abs = ( \ value -> walk e (update env x value) )
                                    in (V2 abs, st)
walk (Eapp e1 e2) env st         = let (ret, st')   = walk e1 env st 
                                       den = ( \ stprm -> walk e2 env stprm )
                                    in d ret (V3 den) st'
walk (Evar x) env st             = e (env x) st
walk (Econst x) env st           = (V1 x, st)
walk (Ein e) env (r : inr, outr) = walk (Eapp e (Econst r)) env (inr, outr)
walk (Eout e1 e2) env st         = let (ret, (inr', outr')) = walk e1 env st 
                                    in walk e2 env (inr', (c ret) : outr')

main = do
  s <- getLine
  let ast = read s
  input <- getLine
  let (_, (_, output)) = walk ast (\x -> error ("error")) (input, "")
  putStrLn $ reverse output
