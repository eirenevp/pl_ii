<!Vlassi-Pand Irene pl2-ex11>
<?php
    if (isset($_POST['play_again'])) {
        session_destroy();
        session_start();
        $_SESSION['start_time'] = time();
        $_SESSION['question_num'] = 1;
    }
    else {
        session_start();
        if (!isset($_SESSION['question_num'])) {
            $_SESSION['start_time'] = time();
            $_SESSION['question_num'] = 1;
        }
        if (($_POST['again'] == 'Continue!') &&  isset($_SESSION['correctness']) && ($_SESSION['correctness'] == 1)) {
            $_SESSION['question_num'] = $_SESSION['lcorrect']+1;
        }    
    }

function time_diff_conv($start, $s) {
    $t = array( //suffixes
        'd' => 86400,
        'h' => 3600,
        'm' => 60,
    );
    $s = abs($s - $start);
    foreach($t as $key => &$val) {
        $$key = floor($s/$val);
        $s -= ($$key*$val);
        $string .= ($$key==0) ? '' : $$key . "$key ";
    }
    return $string . $s. 's';
}
?>
<html>
<head>
<style>
h1 {
    text-align: center;
    font-family: "Times New Roman";
    font-size: 40px;
    font-style: oblique;
    color: #f4bf75;
}

body {
    background-color: #000000;
    font-style: oblique;
    color: #f4bf75;
    font-family: "Times New Roman";
}

code {
    display: block;
    border: 3px solid #f4bf75;
    padding: 5px;
    background-color: #f5f5f5;
    color: #000000;
    width: 95%
}

.question {
    font-size: 24px;
}

.message {
    font-size: 20px;
}

textarea.wide {
    font-size: 20px;
    border: 3px solid #f4bf75;
    padding: 5px;
    background-color: #f5f5f5;
    color: #000000;
    width: 96%;
    height: 100px;
}

input {
    background-color: #f4bf75;
    border: 3px solid #f5f5f5;
    font-size: 15px;
    padding: 5px;
}

</style>
</head>

<body>
<h1>Morse Game - Code Game!</h1>
<p><span class="question">Question <?php echo $_SESSION['question_num'] ?> </span>:</p>

<?php
function encode($str) {
    $encoded = array(
        "." => ".-.-.-",
        "," => "--..--",
        "?" => "..--..",
        "=" => "-...-",
        " " => "",
        "A" => ".-",
        "B" => "-...",
        "C" => "-.-.",
        "D" => "-..",
        "E" => ".",
        "F" => "..-.",
        "G" => "--.",
        "H" => "....",
        "I" => "..",
        "J" => ".---",
        "K" => "-.-",
        "L" => ".-..",
        "M" => "--",
        "N" => "-.",
        "O" => "---",
        "P" => ".--.",
        "Q" => "--.-",
        "R" => ".-.",
        "S" => "...",
        "T" => "-",
        "U" => "..-",
        "V" => "...-",
        "W" => ".--",
        "X" => "-..-",
        "Y" => "-.--",
        "Z" => "--..",
        "0" => "-----",
        "1" => ".----",
        "2" => "..---",
        "3" => "...--",
        "4" => "....-",
        "5" => ".....",
        "6" => "-....",
        "7" => "--...",
        "8" => "---..",
        "9" => "----." 
    );

    for($i=0; $i<strlen($str); ++$i) {
        echo $encoded[$str[$i]] . " ";
    }
}
    $strings = array(
        1 => "HELLO WORLD",
        2 => "THE ANSWER TO LIFE THE UNIVERSE AND EVERYTHING STOP",
        3 => "LEONARDO DA VINCI",
        4 => "LEONARDO DA VINCI WAS CONCERNED WITH THE LAWS OF SCIENCE AND NATURE WHICH GREATLY INFORMED HIS WORK AS A PAINTER SCULPTOR INVENTOR AND DRAFTSMEN STOP",
        5 => "ITS BEEN SAID THAT THE MONA LISA HAD JAUNDICE THAT SHE WAS A PREGNANT WOMAN AND THAT SHE WASNT ACTUALLY A WOMAN AT ALL BUT A MAN IN DRAG STOP BASED ON ACCOUNTS FROM AN EARLY BIOGRAPHER HOWEVER THE MONA LISA IS A PICTURE OF LISA GIOCONDA THE REAL-LIFE WIFE OF A MERCHANT BUT THATS FAR FROM CERTAIN STOP FOR DA VINCI THE MONA LISA WAS FOREVER A WORK IN PROGRESS AS IT WAS HIS ATTEMPT AT PERFECTION STOP",
        4 => "LEONARDOS APPROACH TO SCIENCE WAS AN OBSERVATIONAL ONE STOP HE TRIED TO UNDERSTAND A PHENOMENON BY DESCRIBING AND DEPICTING IT IN UTMOST DETAIL AND DID NOT EMPHASIZE EXPERIMENTS OR THEORETICAL EXPLANATION STOP",
        5 => "ONE OF DA VINCIS LAST COMMISSIONED WORKS WAS A MECHANICAL LION THAT COULD WALK AND OPEN ITS CHEST TO REVEAL A BOUQUET OF LILIES STOP",
        6 => "DESPITE THE RECENT AWARENESS AND ADMIRATION OF LEONARDO AS A SCIENTIST AND INVENTOR FOR THE BETTER PART OF FOUR HUNDRED YEARS HIS FAME RESTED ON HIS ACHIEVEMENTS AS A PAINTER AND ON A HANDFUL OF WORKS EITHER AUTHENTICATED OR ATTRIBUTED TO HIM THAT HAVE BEEN REGARDED AS AMONG THE MASTERPIECES STOP",
        7 => "LEONARDO CAN BE CONSIDERED QUITE RIGHTLY TO HAVE BEEN THE UNIVERSAL GENIUS PAR EXCELLENCE AND WITH ALL THE DISQUIETING OVERTONES INHERENT IN THAT TERM STOP"
    );
?>
<?php
    $str = $strings[$_SESSION['question_num']];
    ?>  <code><?php encode($str); ?></code> <?php
    if (isset($_POST['answer'])) {
        if (strtolower($_POST['answer']) == strtolower($str)) {
            if (is_null($strings[$_SESSION['question_num']+1])) {
                ?> <p><span class="message">Congrats!! Morse would be proud... It took you only
                <?php echo time_diff_conv($_SESSION['start_time'], time()); ?>!</span></p> <?php 
                ?> <form action="solveit.php" id="p" name="p" method="post">
                   <input type="submit" name="play_again" id="play_again" value="Play Again!" />
                   </form> <?php
                return;
            }
            else {
                $_SESSION['lcorrect'] = $_SESSION['question_num'];
                $_SESSION['correctness'] = 1;
                ?> </br><span class="message">Right!!! :D</span></p> <?php
            }
        } 
        else {
            $_SESSION['correctness'] = 0;
            ?> </br><span class="message"> Wrong... :/</span></p> <?php
        }
        ?> <form action="solveit.php" id="r" name="r" method="post">
                <input type="hidden" id="again" name="continue" value="continue" />
                <input type="submit" name="again" id="again" value="Continue!" />
           </form> <?php
    }
    else {
        ?> <p><span class="message"> Tick tock... Tick tock...</span></p>
           <form action="solveit.php" id="f" name="f" method="post">
                <textarea class="wide" name="answer" id="answer"></textarea></br></br>
                <input type="submit" name="submit" id="submit" value="Submit!" />
                <input type="reset" value="Reset" />
           </form> <?php
    }
?>
</body>
</html>
