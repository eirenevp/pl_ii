# Vlassi-Pandi Eirini
#       pl2-ex11
# Morse Game - client 

#!/usr/bin/env python
# -*- coding: utf-8

import urllib2
import string 
import mechanize
import cookielib 
from bs4 import BeautifulSoup
from sys import argv

CODE = {u' ':        u' ',                                                         
        u'.----.':   u"'",                                                                                                                                                                                                        
        u'-.--.-':   u'(',                                                         
        u'-.--.-':   u')',                                                         
        u'--..--':   u':',                                                         
        u'-....-':   u'-',                                                         
        u'.-.-.-':   u'.',                                                         
        u'-..-.':    u'/',                                                         
        u'-----':    u'0',                                                         
        u'.----':    u'1',                                                         
        u'..---':    u'2',                                                         
        u'...--':    u'3',                                                         
        u'....-':    u'4',                                                         
        u'.....':    u'5',                                                         
        u'-....':    u'6',                                                         
        u'--...':    u'7',                                                         
        u'---..':    u'8',                                                         
        u'----.':    u'9',                                                         
        u'---...':   u':',                                                         
        u'-.-.-.':   u';',                                                         
        u'..--..':   u'?',                                                         
        u'.-':       u'a',                                                         
        u'-...':     u'b',                                                         
        u'-.-.':     u'c',                                                         
        u'-..':      u'd',                                                         
        u'.':        u'e',                                                         
        u'..-.':     u'f',                                                         
        u'--.':      u'g',                                                         
        u'....':     u'h',                                                         
        u'..':       u'i',                                                         
        u'.---':     u'j',                                                         
        u'-.-':      u'k',                                                         
        u'.-..':     u'l',                                                         
        u'--':       u'm',                                                         
        u'-.':       u'n',                                                         
        u'---':      u'o',                                                         
        u'.--.':     u'p',                                                         
        u'--.-':     u'q',                                                         
        u'.-.':      u'r',                                                         
        u'...':      u's',                                                         
        u'-':        u't',                                                         
        u'..-':      u'u',                                                         
        u'...-':     u'v',                                                         
        u'.--':      u'w',                                                         
        u'-..-':     u'x',                                                         
        u'-.--':     u'y',                                                         
        u'--..':     u'z',                                                         
        u'..--.-':   u'_'}

def repair(spCode):
    fixed = string.replace(spCode, u'·', u'.')
    fixed  = string.replace(fixed, u'–', u'-')
    return fixed.encode('latin1', 'ignore')

def decodeWord(fxCode):
    letters = fxCode.split()
    dcCode = ""
    for letter in letters:
        dcCode += CODE[letter]
    return dcCode

def decode(fxCode):
    words = fxCode.split("  ")
    dcCode = ""
    for word in words:
        dcCode += decodeWord(word)+" "
    return dcCode.strip()

def rawDecode(data):
    soupa = BeautifulSoup(data)
    spCode = soupa.find_all('code')[0]
    return decode(repair(spCode.text))

def mechBrowser():
    # Browser
    br = mechanize.Browser()

    # Cookie Jar
    cj = cookielib.LWPCookieJar()
    br.set_cookiejar(cj)

    # Browser options
    br.set_handle_equiv(True)
    br.set_handle_redirect(True)
    br.set_handle_referer(True)
    br.set_handle_robots(False)

    # Follows refresh 0 but not hangs on refresh > 0
    br.set_handle_refresh(mechanize._http.HTTPRefreshProcessor(), max_time=1)

    # User-Agent (this is cheating, ok?)
    br.addheaders = [('User-agent', 'Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.1) Gecko/2008071615 Fedora/3.0.1-1.fc9 Firefox/3.0.1')]
    return br

def continuePlay(data):
    soupa = BeautifulSoup(data)
    spCnt = soupa.find("input", {"type": "submit"})
    return spCnt != None and spCnt['value'] == "Continue!"

def main():
    browser = mechBrowser()
    ret = browser.open(argv[1])
    while True:
        rawdata = ret.read() 
        answer = rawDecode(rawdata)
        print answer
        browser.select_form(nr = 0)
        browser["answer"] = answer
        ret = browser.submit()
        if continuePlay(ret.read()):
            browser.select_form(nr = 0)
            ret = browser.submit()
        else:
            break


if __name__=="__main__":
    main()
