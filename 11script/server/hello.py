#!/usr/bin/env python
# -*- coding: utf-8


from bottle import route, request, view, run, template


@route('/')
@view('hello_template')
def hello(name='World'):
    return template('hello_template', **locals())


run(host='localhost', port=8080, debug=True)
