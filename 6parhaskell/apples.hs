import Data.List
import Data.List (permutations)
import Data.List (maximum)
import Data.List (concat)
import Data.List (sortBy)
import Data.Char
import Data.Char (isSpace)
import qualified Data.Set as Set
import qualified Data.ByteString as BS
import qualified Data.ByteString.Char8 as BSC
import qualified Data.List.Key as K
import Data.Map ((!), toList, fromList, fromListWith, adjust, keys, Map)

import Control.Monad (sequence)
import Control.Monad.Par

import Control.Parallel.Strategies
import Control.DeepSeq

buildGraph :: Ord a => [(a, a, Int)] -> Map a [(a, Int)]
buildGraph g = fromListWith (++) $ g >>=
               \(a,b,d) -> [(a,[(b,d)]), (b,[(a,d)])]

--dijkstra :: Ord a => a -> Map a [(a, Int)] -> Map a (Int, Maybe a)
dijkstra source graph =
    f (fromList [(v, (if v == source then 0 else 10000000000000000, Nothing)) 
                | v <- keys graph]) (keys graph) where
    f ds [] = [(source, theta, d) | (theta, (d,_)) <- (toList ds)]
    f ds q  = f (foldr relax ds $ graph ! m) (delete m q) where
              m = K.minimum (fst . (ds !)) q
              relax (e,d) = adjust (min (fst (ds ! m) + d, Just m)) e

tuplify3 :: [a] -> (a, a, a)
tuplify3 [x,y,z] = (x, y, z)

tuplify [] = []
tuplify (x : xs) = (tuplify3 x : tuplify xs)

thd3 :: (a, b, c) -> c
thd3  (a, b, c) = c

-- this will return the set that can be watered if you put a drill with
-- distance d to position theta
countPoss possWatered (theta, drill) = 
  Set.fromList [m | (m,d) <- (Set.elems (possWatered ! theta)), d <= drill]

-- this returns the number of apples you can water if you put drills as defined
-- in tdrest [(position, drillsize)]
countAll possWatered tdrest = 
  countAllH possWatered tdrest (Set.fromList [])
  where
    countAllH possWatered [] finalset = 
      Set.size finalset
    countAllH possWatered (td : rest) oldset = 
      let newset = Set.union oldset (countPoss possWatered td)
        in
          countAllH possWatered rest newset


eqPerms [] = [[]]
eqPerms xs = [x:xt | x <- nub xs, xt <- eqPerms $ delete x xs] 

solver :: [Int] -> Map Int (Set.Set (Int, Int)) -> Int
solver maxdist possWatered =
    solverH permkeys possWatered combos
  where
    solverH permkeys possWatered combos = 
      Data.List.maximum [countAll possWatered (zip permkeys c) | c <- combos]
    permkeys = keys possWatered
    permutable = (take ((length permkeys) - (length maxdist)) [-1,-1..]) ++ (sort maxdist)
    combos = eqPerms permutable

main :: IO ()
main = 
  do all <- BS.getContents
     let Just (n_trees, r1) = readInt all
     let Just (n_channels, r2) = readInt r1
     let Just (n_apples, r3) = readInt r2
     let Just (n_drills, r4) = readInt r3
     let (channels, r5)  = readMany readChannels r4 n_channels
     let (apples, r6)  = readMany readInt r5 n_apples
     let (maxdist, _)  = readMany readInt r6 n_drills
     let graph = buildGraph (tuplify channels)
     let distance_lim = maximum maxdist
     let djk = sortBy (\ x y -> compare (thd3 y) (thd3 x))  $ filter (\ x -> (thd3 x) <= distance_lim) $ concat [dijkstra i graph | i <- apples]
     let possWatered = fromListWith (Set.union) [(theta, (Set.fromList [(m, d)])) | (m, theta, d) <- djk]

     print $ solver maxdist possWatered
  where readInt s = BSC.readInt (BSC.dropWhile isSpace s)
        readMany _ s 0 = ([], s)
        readMany readf s n = case readf s of
          Just (x, r) -> let (xs, t) = readMany readf r (n-1)
                         in  (x : xs, t)
          Nothing     -> ([], s)
        readChannels s = let (xs, r) = readMany readInt s 3
                         in Just(xs, r)
