-module(apples_par).
-export([run/0]).

run() ->%{{{
    [N, _, _, _, Drills, ApplesTr, MaxDist] = parse(),
    Apples = gb_sets:from_list(ApplesTr),
    MaxDrill = lists:max(MaxDist),
    Graph = digraph:new(),
    List = lists:seq(0, N-1),
    lists:foreach(fun(V) -> digraph:add_vertex(Graph, V) end, List),
    lists:foreach(fun(Channel) -> digraph:add_edge(Graph, lists:nth(1, Channel), lists:nth(2, Channel), lists:nth(3, Channel)) end, Drills),
    Watered = possWatered(N-1, N, Graph, gb_trees:empty(), MaxDrill, Apples),
    Id = self(),
    FinRes = solve(Id, Watered, MaxDist, gb_sets:empty(), 0, N),
    io:fwrite("~p~n", [FinRes]). %}}}

parse() ->%{{{
  [N, M, P, B] = parseInNMPB(),
  Drills = parseInDrills(M),
  Apples = parseInApples(P),
  MaxDist = parseInMaxDistances(B),
  [N, M, P, B, Drills, Apples, MaxDist].

parseInDrills(M) -> 
    parseInDrills(M, []).
parseInDrills(0, Acc) -> 
    lists:reverse(Acc);
parseInDrills(M, Acc) ->
    {ok, [U, V, D]} = io:fread("", "~d~d~d"),
    parseInDrills(M - 1, [[U, V, D] | [[V, U, D] | Acc]]).

parseInApples(P) ->
    parseInApples(P, []).
parseInApples(0, Acc) ->
    lists:reverse(Acc);
parseInApples(P, Acc) ->
    {ok, [X]} = io:fread("", "~d"),
    parseInApples(P - 1, [X | Acc]).

parseInMaxDistances(B) ->
    parseInMaxDistances(B, []).
parseInMaxDistances(0, Acc) ->
    lists:reverse(Acc);
parseInMaxDistances(B, Acc) ->
    {ok, [X]} = io:fread("", "~d"),
    parseInMaxDistances(B - 1, [X | Acc]).

parseInNMPB() ->
  {ok, [N, M, P, B]} = io:fread("", "~d~d~d~d"),
  [N, M, P, B].%}}}

possWatered(-1, _, _, Watered, _, _)  -> Watered;%{{{
possWatered(Source, N, G, Watered, MaxDrill, Apples) ->
    Queue = gb_sets:insert({0, Source}, gb_sets:new()),
    DistDictInit = dict:from_list(lists:map(fun(X) -> {X, inf} end, lists:seq(0, N-1))),
    DistDict = dict:store(Source, 0, DistDictInit),
    Djk = dict:to_list(dijkstra(G, DistDict, Queue, MaxDrill)),
    FilterDjk = lists:filter(fun({Nod, D}) -> gb_sets:is_member(Nod, Apples) andalso D =< MaxDrill  end, Djk),
    SortDist = lists:sort(fun({_,X},{_,Y}) -> X < Y end, FilterDjk),
    Results = lists:map(fun({X,Y}) -> {Y, X} end, SortDist),
    NewWatered = gb_trees:insert(Source, Results, Watered),
    possWatered(Source-1, N, G, NewWatered, MaxDrill, Apples).%}}}
   
relax(Drill, {Dist, Q}) ->%{{{
    {_, V1, V2, W} = Drill,
    CurDist = dict:fetch(V1, Dist) + W,
    OldDist = dict:fetch(V2, Dist),
    if
        CurDist < OldDist ->
            NewDist = dict:store(V2, CurDist, Dist),
            NQ = gb_sets:del_element({OldDist, V2}, Q),
            NewQ = gb_sets:insert({CurDist, V2}, NQ);
        true ->
            NewDist = Dist,
            NewQ = Q
    end,
    {NewDist, NewQ}.%}}}

dijkstra(G, Dist, Q, MD) ->%{{{
    case gb_sets:is_empty(Q) of
        true ->
            Dist;
        false ->
            {{_, Node}, Q2} = gb_sets:take_smallest(Q),
            RealMinDist = dict:fetch(Node, Dist),
            if
                RealMinDist >= MD ->
                    dijkstra(G, Dist, Q2, MD);
                true ->
                    Neighbours = digraph:out_edges(G, Node),
                    {NewDist, NewQ} = lists:foldr(fun(X,Y) -> relax(digraph:edge(G, X), Y) end, {Dist, Q2}, Neighbours),
                    dijkstra(G, NewDist, NewQ, MD)
            end
    end.%}}}

solve(ParentId, Watered, [CurDrill|Drills], CurrentSet, Counter, N) -> %{{{
    case N /= Counter of
        true ->
            FetSet = gb_sets:from_list(lists:map(fun({_, Y}) -> Y end, lists:filter(fun({X, _}) -> X =< CurDrill end, gb_trees:get(Counter, Watered)))), 
            NewSet = gb_sets:union(CurrentSet, FetSet),
            ParId = self(),
            spawn(fun() -> case length([CurDrill|Drills]) of 
                            1 -> ParId ! gb_sets:size(NewSet);
                            _Else -> solve(ParId, Watered, Drills, NewSet, 0, N) end 
                  end),
            spawn(fun() -> solve(ParId, Watered, [CurDrill|Drills], CurrentSet, Counter+1, N) end),
            receive
              Res1 -> Res1
            end,
            receive
              Res2 -> Res2
            end,
            Ans = max(Res1, Res2),
            ParentId ! Ans;
        false ->
            Ans = case length(Drills) of 
                     0 -> gb_sets:size(CurrentSet);
                     _Else -> 0
                end,
            ParentId ! Ans,
            Ans
    end.%}}}


