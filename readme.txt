Tested in Python 2.6.2
Non standard libraries needed: None
Standard libraries needed: re, sys, urllib

If no argument is given, the script will stop when the first bonus image is downloaded.
If any argument is given, the script will never stop.
