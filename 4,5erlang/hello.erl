-module(hello).
-export([run/0]).

-ifndef(PRINT).
-define(PRINT(Var), io:format("DEBUG: ~p:~p - ~p~n~n ~p~n~n", [?MODULE, ?LINE, ??Var, Var])).
-endif.

get_set(Set, Limit) -> gb_sets:from_list(
        lists:map(fun({_, Y}) -> Y end,
                lists:filter(fun({X, _}) -> X =< Limit end, Set))).

solve(_, N, Drills, CurrentSet, Index) when N == Index ->
    case length(Drills) of
        0 -> gb_sets:size(CurrentSet);
        _Else -> 0
    end;
solve(Reachable, N, Drills, CurrentSet, Index) when N /= Index ->
    NewSet = gb_sets:union(CurrentSet, get_set(gb_trees:get(Index, Reachable), hd(Drills))),
    Ans = case length(Drills) of
        1 -> gb_sets:size(NewSet);
        _Else -> solve(Reachable, N, tl(Drills), NewSet, 0)
    end,
    max(solve(Reachable, N, Drills, CurrentSet, Index+1), Ans).

run() ->%{{{
    {ok, [N, M, P, B]} = io:fread("", "~d ~d ~d ~d"),
    Edges = readedges(M),
    Trees = readlist(P),
    Drills = readlist(B),
    MaxDrill = lists:max(Drills),
    ?PRINT(Edges),
    ?PRINT(Trees),
    ?PRINT(Drills),
    Graph = digraph:new(),
    addVertices(Graph, N),
    addEdges(Graph, Edges),
    Reachable = lists:map(fun(Ap) -> getAllReachable(Ap, N, Graph, gb_trees:empty(), MaxDrill, Trees) end, Trees),
    ?PRINT(Reachable).
    %?PRINT(solve(Reachable, N, Drills, gb_sets:empty(), 0)). %}}}
    

%getAllReachable(-1, _, _, Reachable, _, _)  -> Reachable;%{{{
getAllReachable(Source, N, Graph, Reachable, MaxDrill, Trees) ->
            Queue = gb_sets:insert({0, Source}, gb_sets:new()),
            DistancesEmpty = dict:from_list(lists:map(fun(X) -> {X, inf} end, lists:seq(0, N-1))),
            DD = dict:store(Source, 0, DistancesEmpty),
            Djk = dict:to_list(dijkstra(Graph, DD, Queue, MaxDrill, Trees)),
            Results = cleardists(Djk),
            ?PRINT(Results),
            %Results = dict:to_list(dijkstra(Graph, DD, Queue, MaxDrill)),
            NewReachable = gb_trees:insert(Source, Results, Reachable).
            %getAllReachable(Source-1, N, Graph, NewReachable, MaxDrill, Trees).
    %}}}
cleardists(List) ->%{{{
    FilteredList = lists:filter(fun({N, D}) ->  end, List),
    ReversedList = lists:map(fun({N, D}) -> {D, N} end, List),
    lists:sort(ReversedList).%}}}
relax(Edge, {Distances, Q}) ->%{{{
    {_, V1, V2, Weight} = Edge,
    CurDist = dict:fetch(V1, Distances) + Weight,
    OldDist = dict:fetch(V2, Distances),
    if
        OldDist == -1 orelse CurDist < OldDist ->
            NewDistances = dict:store(V2, CurDist, Distances),
            NewQ = gb_sets:insert({CurDist, V2}, Q);
        true ->
            NewDistances = Distances,
            NewQ = Q
    end,
    {NewDistances, NewQ}.%}}}
dijkstra(Graph, Distances, Q, MD, Tr) ->%{{{
    case gb_sets:is_empty(Q) of
        true ->
            Distances;
        false ->
            {{Distance, Node}, Q2} = gb_sets:take_smallest(Q),
            RealMinDist = dict:fetch(Node, Distances),
           % AppleTree = (gb_sets:is_member(Node, Tr)),
            if
                RealMinDist >= MD orelse RealMinDist /= Distance ->
                    dijkstra(Graph, Distances, Q2, MD, Tr);
                true ->
                    Neighbours = digraph:out_edges(Graph, Node),
                    {NewDistances, NewQ} = lists:foldl(fun(X,Y) -> relax(digraph:edge(Graph, X), Y) end, {Distances, Q2}, Neighbours),
                    dijkstra(Graph, NewDistances, NewQ, MD, Tr)
            end
    end.%}}}

addVertices(G, N) when N > 0 ->%{{{
                digraph:add_vertex(G, N-1),
                addVertices(G, N-1);
            addVertices(_, 0) -> ok.%}}}
addEdges(G, [[X, Y, Z]|Tail]) ->%{{{
                digraph:add_edge(G, X, Y, Z),
                addEdges(G, Tail);
            addEdges(_, []) -> ok.%}}}

% TODO: Convert both to tail recursive
readlist(N) when N > 0 ->%{{{
                {ok, [X]} = io:fread("", "~d"),
                [X|readlist(N-1)];
            readlist(0) -> [].%}}}
readedges(M) when M > 0 ->%{{{
                {ok, [X, Y, Z]} = io:fread("", "~d ~d ~d"),
                [[X, Y, Z]|[[Y, X, Z]|readedges(M-1)]];
            readedges(0) -> [].%}}}
