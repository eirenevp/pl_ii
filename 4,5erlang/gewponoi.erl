-module(gewponoi).
-export([run/0]).

-ifndef(PRINT).
-define(PRINT(Var), io:format("DEBUG: ~p:~p - ~p~n~n ~p~n~n", [?MODULE, ?LINE, ??Var, Var])).
-endif.

run() -> 
  [N, M, P, B, Drills, Apples, MaxDist] = parse(),
  MaxDrill = lists:max(Drills),
  Graph = digraph:new().
  Data = list_to_tuple(lists:seq(0, N-1)),
  digraph:add_vertex(Graph, Data),
  lists:foreach(fun(Channel) -> digraph:add_edge(Graph, lists:nth(2, Channel), lists:nth(1, Channel)) end, Drills),
  lists:foreach(fun(Channel) -> digraph:add_edge(Graph, lists:nth(1, Channel), lists:nth(2, Channel)) end, Drills),  
  dijkstra(
parse() ->%{{{
  [N, M, P, B] = parseInNMPB(),
  Drills = parseInDrills(M),
  Apples = parseInApples(P),
  MaxDist = parseInMaxDistances(B),
  [N, M, P, B, Drills, Apples, MaxDist].

parseInDrills(M) -> 
    parseInDrills(M, []).
parseInDrills(0, Acc) -> 
    lists:reverse(Acc);
parseInDrills(M, Acc) ->
    {ok, [U, V, D]} = io:fread("", "~d~d~d"),
    parseInDrills(M - 1, [[U, V, D] | Acc]).

parseInApples(P) ->
    parseInApples(P, []).
parseInApples(0, Acc) ->
    lists:reverse(Acc);
parseInApples(P, Acc) ->
    {ok, [X]} = io:fread("", "~d"),
    parseInApples(P - 1, [X | Acc]).

parseInMaxDistances(B) ->
    parseInMaxDistances(B, []).
parseInMaxDistances(0, Acc) ->
    lists:reverse(Acc);
parseInMaxDistances(B, Acc) ->
    {ok, [X]} = io:fread("", "~d"),
    parseInMaxDistances(B - 1, [X | Acc]).

parseInNMPB() ->
  {ok, [N, M, P, B]} = io:fread("", "~d~d~d~d"),
  [N, M, P, B].%}}}

relax(E, {Dist, Q}) ->%{{{
    {_, V1, V2, W} = E,
    CurDist = dict:fetch(V1, Dist) + W,
    if 
        CurDist < dict:fetch(V2, Dist) ->
            Dist = dict:store(V2, CurDist, Dist),
            Q = gb_sets:insert({CurDist, V2}, Q);
    end,
    {Dist, Q}.%}}}
 
dijkstra(Source, Graph) ->
    PrioQ = gb_sets:insert({0, Source}, gb_sets:empty()),
    InitDist = dict:from_list(lists:map(fun(X) -> {X, inf} end, lists:seq(0, N-1)),
    InitDist = dict:update(Source, 0, InitDist),
    dijkstra(Graph, PrioQ, InitDist).

dijkstra(G, PQ, Dist) ->
    case gb_sets:is_empty(PQ) of
        true -> 
            Dist;
        false ->
            lists:sort(fun({_,X},{_,Y}) -> X < Y end, dict:to_list(Dist));
        
addinf(inf, _) -> inf;
addinf(_, inf) -> inf;
addinf(X, Y)   -> X + Y.

    Results = lists:map(fun({X,Y}) -> {Y, X} end, SortDist),

