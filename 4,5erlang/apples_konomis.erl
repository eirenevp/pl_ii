-module(apples_konomis).
-compile(export_all).

-define(INF, 10000000000).

init_graph(-1, T) -> T;
init_graph(N, T) ->
	T1 = gb_trees:insert(N, [], T),
	init_graph(N-1, T1).

init_queue(-1, _, Q) -> Q;
init_queue(N, P, Q) -> 
		case N of
		     P -> init_queue(N-1, P, Q);
		     _ -> init_queue(N-1, P, gb_trees:insert(N, 0, Q))
		end.

init_distance(-1, _, _) -> ok;
init_distance(N, P, DV) -> 
	case N of
	     P -> 
		  ets:insert(DV, {N, 0}),
		  init_distance(N-1, P, DV);
	     _ -> 
		  ets:insert(DV, {N, 10000000000}),
		  init_distance(N-1, P, DV)
	end.	
				
update(Q, _, [],  _) -> Q;
update(Q, DV, [{V, W} | VDs], Du) ->
	DuW = Du+W,
        Dv = ets:lookup_element(DV, V, 2),
	case (Dv =< DuW) of
		true -> update(Q, DV, VDs, Du);
	   	false -> 
			 NDv = gb_trees:get(Dv, Q),
                   	 T1 = gb_trees:delete(V, NDv),
                   	 case gb_trees:is_empty(T1) of
			 	true -> Q1 = gb_trees:delete(Dv, Q);
				false -> Q1 = gb_trees:update(Dv, T1, Q)
			 end,
                   	 case gb_trees:lookup(DuW, Q1) of
                        	{value, NDuW} -> T2 = gb_trees:insert(V, 0, NDuW),
                                                 Q2 = gb_trees:update(DuW, T2, Q1);
                        	none -> E = gb_trees:empty(),
                                	T2 = gb_trees:insert(V, 0, E),
                                	Q2 = gb_trees:insert(Du+W, T2, Q1)
                   	 end,
                   	 ets:update_element(DV, V, {2, Du+W}),
                   	 update(Q2, DV, VDs, Du)
        end.

init_dijkstra(N, P) ->
        E = gb_trees:empty(),
        T = gb_trees:insert(10000000000, init_queue(N, P, E), E),
        Q = gb_trees:insert(0, gb_trees:insert(P, 0, E), T),
        DV = ets:new(distance_vector, [set]),
        init_distance(N, P, DV),
        {Q, DV}.

core_dijkstra(G, Q, DV, P, B, DsAcc, KAcc) -> 
	case gb_trees:is_empty(Q) of
		true -> {DsAcc, KAcc};
		false ->  
			{Min, M1} = gb_trees:smallest(Q),
			if (Min > B) -> {DsAcc, KAcc};
			   true ->	
				{U, 0} = gb_trees:smallest(M1),
				M2 = gb_trees:delete(U, M1),
				case gb_trees:is_empty(M2) of
					true -> Q1 = gb_trees:delete(Min, Q);
					false -> Q1 = gb_trees:update(Min, M2, Q)
				end,
				Neighbors = gb_trees:get(U, G),
				Q2 = update(Q1, DV, Neighbors, Min),
				core_dijkstra(G, Q2, DV, P, B, [{P, U, Min} | DsAcc], ordsets:add_element(U, KAcc))
			end	
	end.						 

dijkstra(G, N, P, B) -> 
	{Q, DV} = init_dijkstra(N, P),
	{DVp, Kp} = core_dijkstra(G, Q, DV, P, B, [], ordsets:new()),
	{DVp, Kp}.

candidates(_, _,  [], _, DsAcc, KAcc) -> {DsAcc, KAcc};
candidates(G, N, [P|Ps], B, DsAcc, KsAcc) ->
	{DVp, Kp} = dijkstra(G, N, P, B),
	candidates(G, N, Ps, B, DVp++DsAcc, ordsets:union(KsAcc, Kp)). 

apples_watered_aux(_, [], _) -> ok;		     	
apples_watered_aux(B, [{P,K,D}|PKDs], GT) ->
	case (D =< B) of
		true -> case ets:lookup(GT, {K,B}) of
				[] -> AppB = ordsets:add_element(P, ordsets:new()),
				      ets:insert(GT, {{K,B}, AppB});
		    [{{K,B}, AppB}]-> NAppB = ordsets:add_element(P, AppB),
				      ets:update_element(GT, {K,B}, {2, NAppB})	
			end,
			apples_watered_aux(B, PKDs, GT); 	 
		false-> apples_watered_aux(B, PKDs, GT)
	end.

apples_watered([], _, _, _) -> ok;
apples_watered([B|Bs], SBs, PKDs, GT) ->
	case lists:member(B, SBs) of 
		true -> apples_watered(Bs, SBs, PKDs, GT);
		false-> apples_watered_aux(B, PKDs, GT),
			apples_watered(Bs, [B|SBs], PKDs, GT)
	end.
	
perms([]) -> [[]];
perms(L) -> [[X|T] || X <- L, T <- perms(L -- [X])].

mul_aux(_, [], Acc) -> Acc;
mul_aux(K1, [K2|K2s], Acc) ->
        [K2H|_] = K2,
        case (K1 < K2H) of
                true -> mul_aux(K1, K2s, [[K1|K2]|Acc]);
                false-> mul_aux(K1, K2s, Acc)
        end.

mul([], _, Acc) -> Acc;
mul([K1|K1s], K2, Acc) ->
        NAcc = mul_aux(K1, K2, []),
        mul(K1s, K2, NAcc ++ Acc).

product(0, _, LP) -> LP;
product(B, K, LP) ->
        NP = mul(K, LP, []),
        product(B-1, K, NP).

max_apples_aux([], [], _, App) -> ordsets:size(App);
max_apples_aux([K|Ks], [B|Bs], GT, App) ->
	case ets:lookup(GT, {K,B}) of
		[] -> max_apples_aux(Ks, Bs, GT, App);
	[{{K,B}, AppKB}] -> max_apples_aux(Ks, Bs, GT, ordsets:union(App, AppKB))
	end.

max_apples(_, [], _, Max) -> Max;
max_apples(Ks, [PB|PBs], GT, Max) ->
	PMax = max_apples_aux(Ks, PB, GT, ordsets:new()),
	max_apples(Ks, PBs, GT, max(Max, PMax)). 

opt([], _, _, Max) -> Max;
opt([C|Cs], PBs, GT, Max) ->
	PMax = max_apples(C, PBs, GT, Max),
	opt(Cs, PBs, GT, max(Max, PMax)).

format(0, Acc) -> Acc;
format(N, Acc) -> format(N-1, "~d" ++ Acc).

read_edges(0, G) -> G;
read_edges(M, G) ->
        {ok, [U, V, D]} = io:fread([], "~d~d~d"),
        AdjU = gb_trees:get(U, G),
        G1 = gb_trees:delete(U, G),
        G2 = gb_trees:insert(U, [{V,D} | AdjU], G1),
        AdjV = gb_trees:get(V, G2),
        G3 = gb_trees:delete(V, G2),
        G4 = gb_trees:insert(V, [{U,D} | AdjV], G3),
        read_edges(M-1, G4).

apples() ->
        {ok, [N, M, P, B]} = io:fread([], "~d~d~d~d"),
        IG = init_graph(N-1, gb_trees:empty()),
        G = read_edges(M, IG),
        {ok, Ps} = io:fread([], format(P, [])),
        {ok, Bs} = io:fread([], format(B, [])),
	PBs = gb_sets:to_list(gb_sets:from_list(perms(Bs))),
        {DV, Ks} = candidates(G, N-1, Ps, lists:max(Bs), [], ordsets:new()),
        GT = ets:new(gt, [set]),
	apples_watered(Bs, [], DV, GT),
	K = ordsets:to_list(Ks), LK = lists:map(fun(X) -> [X] end, K),
        Cs = product(B-1, K, LK), 	
%	Opt = opt(Cs, PBs, GT, 0),
 	io:fwrite("~.B ~n", [length(Cs)]),   
        io:fwrite("~.B", [length(PBs)]).
%	io:fwrite("~.B", [length(PBs)]). 
