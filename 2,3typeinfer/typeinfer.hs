-        Irene Vlassi-Pandi (03108137)       -}
{-              ex2-plII-2012                -}

import Data.Char
import Data.List
import System.IO

data Type  =  Tvar Int | Tfun Type Type | Terror               deriving Eq
data Expr  =  Evar String | Eabs String Expr | Eapp Expr Expr  deriving Eq

-- Pretty printing of expressions

always = True    -- False omits parentheses whenever possible

instance Show Expr where
  showsPrec p (Evar x) = (x ++)
  showsPrec p (Eabs x e) =
    showParen (always || p > 0) ((("\\" ++ x ++ ". ") ++) . showsPrec 0 e)
  showsPrec p (Eapp e1 e2) =
    showParen (always || p > 1) (showsPrec 1 e1 . (" " ++) . showsPrec 2 e2)

-- Parsing of expressions

instance Read Expr where
  readsPrec _ s =
    readParen True (\s ->
      [(Eabs x e, r)    |  ("\\", t1)  <-  lex s,
                           (x, t2)     <-  lex t1, isVar x,
                           (".", t3)   <-  lex t2,
                           (e, r)      <-  readsPrec 0 t3] ++
      [(Eapp e1 e2, r)  |  (e1, t)     <-  readsPrec 0 s,
                           (e2, r)     <-  readsPrec 0 t]) s ++
      [(Evar x, r)      | (x, r) <- lex s, isVar x]
        where isVar x = isAlpha (head x) && all isAlphaNum x

-- Pretty printing of types

instance Show Type where
  showsPrec p (Tvar alpha) = ("@" ++) . showsPrec 0 alpha
  showsPrec p (Tfun sigma tau) =
    showParen (p > 0) (showsPrec 1 sigma . (" -> " ++) . showsPrec 0 tau)
  showsPrec p (Terror) = showString "type error"

-- Main program

readOne  =  do  s <- getLine
                let e = read s :: Expr
                print (fst_3(finaltype e))

count n m   =  sequence $ take n $ repeat m

fst_3(x, _, _) = x

getTypeConstrains :: Expr -> [(String, Type)] -> Int -> (Type, [(Type,Type)], Int)
getTypeConstrains (Evar x) ((v,t):vars) n 
    | x == v = (t, [], n)
    | otherwise = getTypeConstrains (Evar x) vars n

getTypeConstrains (Eabs x e) vars n =  
    let
        t = Tvar (n+1)
        (t', c', n') = getTypeConstrains e ((x, t):vars) (n+1)
    in
        (Tfun t t',c',n')

getTypeConstrains (Eapp e1 e2) vars n =
    let
        (t1, c1, n1) = getTypeConstrains e1 vars n
        (t2, c2, n2) = getTypeConstrains e2 vars n1
        t3 = Tvar (n2+1)
    in
        (t3, (t1, Tfun t2 t3):(c1++c2), n2+1)

-- W Algorithm
findA a (Tvar b) = a == b
findA a (Tfun t1 t2) = findA a t1 || findA a t2

replaceA _ _ [] = []
replaceA a t ((c1, c2):c) =  
    let 
        replaceAH a t (Tvar b)
            | a == b = t
            | otherwise = Tvar b
        replaceAH a t (Tfun t1 t2) = Tfun (replaceAH a t t1) (replaceAH a t t2)
    in
        ((replaceAH a t c1, replaceAH a t c2):replaceA a t c)

            
unify :: [(Type, Type)] -> Bool -> [(Type, Type)]
unify [] True  = []
unify _  False = [(Terror,Terror)]
unify ((t1,t2):c) flag
    | t1 == t2 = unify c flag
    | otherwise =
        let
            unifyH ((Tfun t11 t12, Tfun t21 t22):c) True = unify ((t11, t21):(t12, t22):c) True
            unifyH ((Tvar a,t2):c) True
                | not (findA a t2) = ((Tvar a, t2):unify (replaceA a t2 c) True)
            unifyH ((t1,Tvar a):c) True
                | not (findA a t1) = ((Tvar a, t1):unify (replaceA a t1 c) True)
            unifyH c True = unify c False
        in  
            unifyH ((t1, t2):c) flag

-- Apply unified constrains to find the type of e but without 'lexical' order
applyUnified [(Terror,Terror)] _ = Terror
applyUnified [] t = t
applyUnified (u:us) t = 
    let
       applyUnifiedH (Tvar a, c) (Tvar b)
            | a == b = c
            | otherwise = Tvar b
       applyUnifiedH cs (Tfun t1 t2) = Tfun (applyUnifiedH cs t1) (applyUnifiedH cs t2)
    in
        applyUnified us (applyUnifiedH u t)

-- 'Lexical' order of the final type 
order :: Type -> [Int] -> Int -> (Type,[Int],Int)
order  Terror  _  _  = (Terror, [], -1)
order (Tvar a) l  n  = 
   let 
       pos = elemIndex a l 
       n'= n+1
       y = case pos of
                Just pos ->  pos
                Nothing  ->  n'
   in
       if (y == n')
       then ((Tvar y) , (l++[a]), n')
       else ((Tvar y),   l      , n )
order (Tfun t1 t2) l n = 
        ((Tfun t1' t2'), l2, n2)
            where (t1', l1, n1) = order t1 l  n  
                  (t2', l2, n2) = order t2 l1 n1 


finaltype e = (order (applyUnified (unify c True) t) [] (-1))
    where (t, c, n) = getTypeConstrains e [] 0


main    =  do  n <- readLn
               count n readOne
                
