type 'a node_t = 
    | Node of 'a * 'a list_t
and 'a list_t = 'a node_t Lazy.t

let zzhead zzList = 
    match Lazy.force zzList with
        | Node(x, _) -> x

let zztail zzList = 
    match Lazy.force zzList with
        | Node(_, nxt) -> Lazy.force nxt

let rec counter i = lazy(Node(i, counter(i+1)))

let rec zztake (c,s,a) = 
    match (c, Lazy.force s, a) with
        | (0, _, acc) -> List.rev acc
        | (i, Node(x, nxt), acc) -> zztake(i-1, nxt, x::acc)

let rec zzfilter(p, t) = 
    match Lazy.force t with
        | Node(x, nxt) -> if p x 
            then lazy(Node(x, zzfilter(p, nxt)))
            else zzfilter(p, nxt)


let sift a ns = zzfilter((fun (n) -> n mod a <> 0), ns)

let rec sieve p = 
    match p with 
        | lazy(Node(n,ns)) -> lazy(Node(n, sieve(sift n ns))) 

