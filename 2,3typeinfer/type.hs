import Char
import IO

data Type  =  Tvar Int | Tfun Type Type | Terror               deriving Eq
data Expr  =  Evar String | Eabs String Expr | Eapp Expr Expr  deriving Eq

-- Pretty printing of expressions

always = True    -- False omits parentheses whenever possible

instance Show Expr where
  showsPrec p (Evar x) = (x ++)
  showsPrec p (Eabs x e) =
    showParen (always || p > 0) ((("\\" ++ x ++ ". ") ++) . showsPrec 0 e)
  showsPrec p (Eapp e1 e2) =
    showParen (always || p > 1) (showsPrec 1 e1 . (" " ++) . showsPrec 2 e2)

-- Parsing of expressions

instance Read Expr where
  readsPrec _ s =
    readParen True (\s ->
      [(Eabs x e, r)    |  ("\\", t1)  <-  lex s,
                           (x, t2)     <-  lex t1, isVar x,
                           (".", t3)   <-  lex t2,
                           (e, r)      <-  readsPrec 0 t3] ++
      [(Eapp e1 e2, r)  |  (e1, t)     <-  readsPrec 0 s,
                           (e2, r)     <-  readsPrec 0 t]) s ++
    [(Evar x, r) | (x, r) <- lex s, isVar x]
      where isVar x = isAlpha (head x) && all isAlphaNum x

-- Pretty printing of types

instance Show Type where
  showsPrec p (Tvar alpha) = ("@" ++) . showsPrec 0 alpha
  showsPrec p (Tfun sigma tau) =
    showParen (p > 0) (showsPrec 1 sigma . (" -> " ++) . showsPrec 0 tau)
  showsPrec p (Terror) = ("type error"++ )

-- Main program

fst_3 (t,_,_) = t

readOne  =  do  s <- getLine
                let e = read s :: Expr
                putStrLn (show  (final_type e))



count n m  =  sequence $ take n $ repeat m



--­expression to type and constraints

e2tc:: Expr->[(String, Type)]->Int->(Type,[(Type,Type)],Int)
e2tc (Evar x) [] n         = (Terror,[],n)
e2tc (Evar x) ((y,t):ts) n = if  (x==y)  then (t,[],n)
                             else        e2tc (Evar x) ts n  
e2tc (Eabs x e) g n = if (t'==Terror)  then  ((Terror),c',n')
                      else             ((Tfun (Tvar n) t'),c',n')  
                  where (t',c',n')= e2tc e ((x,(Tvar n)):g) (n+1)

e2tc (Eapp e1 e2) g n = let 
                            (t2,c2,n2)= e2tc e2 g n
                            (t1,c1,n1)= e2tc e1 g n2
                            t= if ((t1==Terror) || (t2==Terror)) then 
                                     (Terror)
                               else 
                                     (Tvar n1)
                        in   
                            (t, (t1,(Tfun t2 t)):c1++c2, n1+1)

subtype a (Tvar w) = (a==w)
subtype a (Tfun t2 t3) = ((subtype a t2)||(subtype a t3))

replace a t (Tvar w)= if (w==a) then t
                      else (Tvar w)
replace a t (Tfun t1 t2) = (Tfun (replace a t t1) (replace a t t2))


replace_c _ _ [] = []
replace_c a t ((c1,c2):c)= ((replace a t c1,replace a t c2):(replace_c a t c))

unify :: [(Type,Type)]->[(Type,Type)]
unify [] =[]
unify ((t1,t2):c)= if (t1==t2) then unify c
                   else unify2 ((t1,t2):c)
unify2 (((Tfun t1 t2),(Tfun t3 t4)):c) = unify ((t1,t3):((t2,t4):c))

unify2 (((Tvar a),t2):c)= if (subtype a t2 == False) then (((Tvar a),t2):(unify (replace_c a t2 c)))
                          else [(Terror,Terror)]
unify2 ((t2,(Tvar a)):c)= if (subtype a t2 == False) then (((Tvar a),t2):(unify (replace_c a t2 c)))
                          else [(Terror,Terror)]

apply:: (Type,Type)->Type->Type
apply (Tvar x,t) (Tvar y)         = if   (x==y) then t 
                                    else (Tvar y) 
apply x (Tfun sigma tau)          = (Tfun (apply x sigma) (apply x tau))


unilist:: [(Type,Type)]->[(Type,Type)]
unilist s = let
                  unilast [] = []
                  unilast (((Terror),(Terror)):ts) = [((Terror),(Terror))]         
                  unilast (t:ts) = unilast ts
            in
                  if (unilast s == []) then s
                  else [(Terror,Terror)]



apply_unified:: [(Type,Type)]->Type->Type
apply_unified [] t = t
apply_unified [((Terror),(Terror))] _ = (Terror)
apply_unified _ (Terror) = (Terror)
apply_unified (s:ts) t = apply_unified ts (apply s t)


--xoris leksikografiki seira mexri tora
final_type:: Expr->Type
final_type e = fst_3 (lexico (apply_unified (unilist (unify c)) t) 0 [])
          where (t,c,n)= e2tc e [] 0

lexico :: Type -> Int->[(Int,Int)] ->(Type, Int, [(Int,Int)])

lexico (Terror) _ _ = ((Terror),0,[])

lexico (Tvar a) n l= if (u== -1) then ((Tvar n),n',((a,n):l))
                     else ((Tvar u), n, l)
                where u= listpos a l
                      n'=n+1


lexico (Tfun t1 t2) n l= ((Tfun t1' t2'), n'', l'')
                where (t1',n', l')  = lexico t1 n l 
                      (t2',n'',l'') = lexico t2 n' l'


listpos x []= -1
listpos x ((a,b):ts)= if (x==a) then b
                      else listpos x ts 

main     =  do  n <- readLn
                count n readOne
