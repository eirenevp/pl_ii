type 'a node_t = 
	| Empty
	| Node of 'a * 'a list_t
and 'a list_t = 'a node_t Lazy.t

let hd lList = 
	match Lazy.force lList with
	    | Empty -> Empty
	    | Node(x, _) -> x

let tl lList = 
	match Lazy.force lList with
	    | Empty -> Empty
	    | Node(_, xt) -> Lazy.force xt

let rec seqFrom i = lazy(Node(i, seqFrom(i+1)))

let rec takeSeq (c,s,a) = 
	match (c, Lazy.force s, a) with
	     (_, Empty, acc) -> List.rev acc
	    |(0, _, acc) -> List.rev acc
	    |(i, Node(x,xt), acc) -> takeSeq(i-1, xt, x::acc)

let rec filterSeq(p, t) = match Lazy.force t with
	 Empty -> lazy(Empty)
	|Node(x, xt) -> if p x 
			then lazy(Node(x, filterSeq(p, xt)))
			else filterSeq(p, xt)


let sift a ns = filterSeq((fun (n) -> n mod a <> 0), ns)

let rec sieve p = match p with lazy(Node(n,ns)) -> lazy(Node(n, sieve(sift n ns))) 

