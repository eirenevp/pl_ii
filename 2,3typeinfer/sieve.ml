datatype 'a stream_ = Cons of 'a * 'a stream
withtype 'a stream = 'a stream_ SMLofNJ.Susp.susp

fun seq i = Cons (i, SMLofNJ.Susp.delay (fn() => seq (i+1)))

fun seqTake 0 _ = []
  | seqTake n (Cons(x, xs)) = (x :: (seqTake (n-1) (SMLofNJ.Susp.force xs)))

fun seqFilter (Cons(x, xs)) y =
  case (x mod y) of
       0 => seqFilter (SMLofNJ.Susp.force xs) y
     | _ => Cons(x, SMLofNJ.Susp.delay (fn () => seqFilter (SMLofNJ.Susp.force xs) y))

fun sieve( Cons(x, xs) ) = Cons(x, SMLofNJ.Susp.delay (fn() => sieve (seqFilter (SMLofNJ.Susp.force xs) x)))

fun primes n =
  let
    val s = seq 2
  in
    seqTake n (sieve s)
end
