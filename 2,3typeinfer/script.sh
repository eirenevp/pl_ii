#!/bin/bash
ghc typeinfer.hs &&
for i in {1..6}
do
    echo "Running test $i..."
    ./typeinfer < "typeinfer-grader/typeinfer/typeinfer.in$i" > "mytypeinfer.out$i"
    diff -q "mytypeinfer.out$i" "typeinfer-grader/typeinfer/typeinfer.out$i"
done

