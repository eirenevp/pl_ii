#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <iostream>
using namespace std;

#define R 3
#define M 6

// skepsi, cpu1, disk1, disk2.2, disk2.1, cpu2
/* double d[M][R]={{2.5,4,7.5},{20,48,75},{33,57,90},{31,50,95},{14,25,44},{36,57,91}}; */
/* double d[M][R]={{2.5,4,7.5},{16.7,40,62.5},{23.6,40.7,64.3},{31,50,95},{14,25,44},{36,57,91}}; */
double d[M][R]={{2.5,4,7.5},{20,48,75},{33,57,90},{23.8,38.5,73.1},{14,25,44},{36,57,91}};
double n[M][R],r[M][R],l[R],ro[M][R],T[R];
int N[R]={38,67,20};
double approx=0.0001;

double abs(double a,double b) {
    if(a>b) return a-b;
    else return b-a;
}

//ypologismos pollaplasiasti a(k)
double a(int k){
    /* return (k <= 4) ? (0.45+0.55*k) : 2.65; */
    return (k <= 8) ? (0.56+0.44*k) : 4.08;
}
//ypologismos tis katanomis tou synolikou arithmou pelatwn sto stathmo 1
double p(int k,int N_curr){
    if (k==0) {
        double result=0,sum=0;
        double g=1;
        for (int j=0;j<R;j++) result+=l[j]*d[1][j];
        for (int kk=1;kk<=N_curr;kk++){
            g=1;
            for (int ll=1;ll<=kk;ll++){
                g=g*(result/a(ll));
            }
            sum+=g;
        }
        return (1/(1+sum));
    }
    else {
        double result=0;
        for (int j=0;j<R;j++) result+=l[j]*d[1][j];
        result=result/a(k);
        return result*p(k-1,N_curr);
    }
}

int main() {
    double temp;
    double sum_l[R],sum_r[M];
    int cont=1;
    
    //arhikopoiisi n
    for(int i=0;i<M;i++) for(int j=0;j<R;j++) n[i][j]=(double)N[j]/M;
    while(cont){
        cont=0;
        //ypologismos r gia stathmous 0 
        for(int k=0;k<R;k++) {
            r[0][k]=d[0][k];
            sum_l[k]=r[0][k];
            T[k]=r[0][k];
        }
        //ypologismos r gia stathmous 1,2,3,4 (stathmoi anamonis)
        for(int k=0;k<M;k++) sum_r[k]=0;
        for(int i=0;i<M;i++)
            for(int j=0;j<R;j++)
                sum_r[i]+=n[i][j];
        for(int i=1;i<(M-1);i++)
            for(int j=0;j<R;j++){
                r[i][j]=d[i][j]*(1+((N[j]-1)/N[j])*n[i][j]+sum_r[i]-n[i][j]);
                T[j]+=r[i][j];
                sum_l[j]+=r[i][j];
            }
        //ypologismos r gia stathmo 5
        for (int j=0;j<R;j++){
            double s=0;
            for (int k=1;k<=N[j];k++){
                s+=(double)k*p(k-1,N[j])/a(k);
            }
            r[5][j]=d[5][j]*s;
            T[j]+=r[5][j];
            sum_l[j]+=r[5][j];
        }

        //ypologismos l
        for(int j=0;j<R;j++)
            l[j]=N[j]/sum_l[j];
        //eleghos syglisis
        for(int i=0;i<M;i++)
            for(int j=0;j<R;j++){
                ro[i][j]=l[j]*d[i][j];
                temp=l[j]*r[i][j];
                
                if((abs(temp,n[i][j]))>approx){
                    cont=1;
                }
                n[i][j]=temp;

            }
    }
    for(int k=0;k<R;k++) cout << l[k] <<","<<T[k]<< "\n";
    cout<<"ro\n";
    for(int i=0;i<M;i++) for(int j=0;j<R;j++) cout<< "i,j="<< i<<","<<j<<","<<ro[i][j]<<"\n";
    return 0;
}
